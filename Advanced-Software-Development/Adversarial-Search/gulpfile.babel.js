/* eslint-disable @typescript-eslint/no-var-requires */
const gulp = require('gulp');
const sass = require('gulp-sass');
const cleancss = require('gulp-clean-css');
const rename = require('gulp-rename');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const cache = require('gulp-cache');
const browserSync = require('browser-sync').create();
const browserify = require('browserify');
const tsify = require('tsify');
const buffer = require('vinyl-buffer');
const source = require('vinyl-source-stream');
<<<<<<< HEAD
=======
const fs = require('fs');
>>>>>>> Emergent-Computing-For-Optimisation/Coursework

// configure the paths
const devDir = './tmp/';
const buildDir = './build/';

const htmlWatchDir = 'app/html/';

const assetsDestDir = 'assets';

const styleWatchDir = 'app/scss/**/*.scss';
const styleSrcFile = 'app/scss/styling.scss';
const styleDestDir = 'css';

<<<<<<< HEAD
const scriptsWatchDir = 'app/ts/**/*.ts';
=======
const scriptsWatchDir = 'app/ts/';
const scriptsDestDir = 'js';
>>>>>>> Emergent-Computing-For-Optimisation/Coursework

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
function reloadBrowser() {
  cache.clearAll();
  browserSync.reload();
}

gulp.task('serve', () => {
  browserSync.init({
    server: './tmp',
    browser: 'google chrome',
  });
  gulp.watch(htmlWatchDir, gulp.series('dev-html')).on('end', reloadBrowser);
  gulp.watch(styleWatchDir, gulp.series('dev-css')).on('end', reloadBrowser);
  gulp.watch(scriptsWatchDir, gulp.series('dev-js')).on('end', reloadBrowser);
  gulp.watch('/app/assets/**/*', gulp.series('dev-assets')).on('end', reloadBrowser);
});

gulp.task('dev-html', () => gulp.src(['app/html/**/*']).pipe(gulp.dest(devDir)));

gulp.task('dev-assets', () => gulp.src(['app/assets/**/*']).pipe(gulp.dest(devDir + assetsDestDir)));

gulp.task('dev-css', () => gulp.src(styleSrcFile)
  .pipe(sourcemaps.init())
  .pipe(sass({ outputStyle: 'compact', precision: 10, noCache: true })
    .on('error', sass.logError))
  .pipe(sourcemaps.write())
  .pipe(autoprefixer())
  .pipe(rename({
    suffix: '.min',
  }))
  .pipe(gulp.dest(devDir + styleDestDir)));

gulp.task('dev-js', () => browserify({
  basedir: '.',
  debug: true,
  esModuleInterop: true,
  entries: ['app/ts/Application.ts'],
  cache: {},
  packageCache: {},
})
  .plugin(tsify)
  .bundle()
  .pipe(source('app.js'))
  .pipe(buffer())
  .pipe(sourcemaps.init({ loadMaps: true }))
  .pipe(sourcemaps.write('./'))
  .pipe(gulp.dest('tmp/js')));

gulp.task('build-html', () => gulp.src(['app/html/**/*']).pipe(gulp.dest(buildDir)));

gulp.task('build-assets', () => gulp.src(['app/assets/**/*']).pipe(gulp.dest(buildDir + assetsDestDir)));

gulp.task('build-css', () => gulp.src(styleSrcFile)
  .pipe(sourcemaps.init())
  .pipe(sass({ outputStyle: 'compact', precision: 10, noCache: true })
    .on('error', sass.logError))
  .pipe(sourcemaps.write())
  .pipe(autoprefixer())
  .pipe(cleancss())
  .pipe(rename({
    suffix: '.min',
  }))
  .pipe(gulp.dest(buildDir + styleDestDir)));

gulp.task('build-js', () => browserify({
  basedir: '.',
  debug: false,
  esModuleInterop: true,
  entries: ['app/ts/Application.ts'],
  cache: {},
  packageCache: {},
})
  .plugin(tsify)
  .bundle()
  .pipe(source('app.js'))
  .pipe(buffer())
  .pipe(uglify())
  .pipe(gulp.dest('build/js')));

gulp.task('dev', gulp.series('dev-html', 'dev-assets', 'dev-css', 'dev-js'));
gulp.task('build', gulp.series('build-html', 'build-assets', 'build-css', 'build-js'));
