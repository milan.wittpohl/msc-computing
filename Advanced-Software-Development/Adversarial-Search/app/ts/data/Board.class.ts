class Board {
  private numRows: number

  private numColumns: number

  constructor(numRows: number, numColumns: number) {
    this.numRows = numRows;
    this.numColumns = numColumns;
  }
}
