import _ from 'lodash';
import Board from './Board.abstract.class';
import Player from '../Player.class';
import EvaluationService from '../../service/EvaluationService';
import Field from '../Field.class';

export default class TicTacToeBoard extends Board {
  constructor() {
    super(3, 3, 3);
  }

  public cloneBoard(): Board {
    const clone = new TicTacToeBoard();
    _.forEach(this.getFields(), (field) => {
      if (field.isOccupied) {
        const fieldInClone = _.find(clone.getFields(), { x: field.getX(), y: field.getY() }) as Field;
        fieldInClone.assignToPlayer(field.getPlayer());
      }
    });
    return clone;
  }

  // eslint-disable-next-line class-methods-use-this
  public cloneEmptyBoard(): Board {
    const clone = new TicTacToeBoard();
    return clone;
  }

  /**
   * Generally the score is calculated by
   *  -> calculating the number of missing moves for the current player
   *  -> calculating the number of missing moves for the next player (opponent)
   *  -> calculating the score of the current player by
   *        subtracting the number of missing moves from the number of fields needed
   *  -> calculating the score of the next player (opponent) by
   *        subtracting the number of missing moves from the number of fields needed
   * The score is then the score of the player - the score of the opponent
   *    -> Therefore the player tries to maximise the score and the oponnent tries to minimise it
   *
   * This works for most cases, however there are three cases that need special attention:
   *  -> The opponent moves next and is missing only one move -> second worst score for player
   *  -> The player moves next and is missing only one move -> second best score for player
   *  -> The player wins by making this move -> best score
   *  -> The opponent wins by making this move -> worst score
   *
   * @param player
   * @param opponent
   * @param opponentMovesNext -> does the opponent move next
  */
  // eslint-disable-next-line class-methods-use-this
  public evaluateBoardStateForPlayer(player: Player, opponent: Player, opponentMovesNext: boolean): number {
    // General cacluation of score
    const missingCombinationsForPlayer = EvaluationService.calculateMissingWinningCombinationsForPlayer(this, player, opponent);
    const missingCombinationsForOpponent = EvaluationService.calculateMissingWinningCombinationsForPlayer(this, opponent, player);
    const isolatedScorePlayer = this.getNumberToWin() - missingCombinationsForPlayer;
    const isolatedScoreOpponent = this.getNumberToWin() - missingCombinationsForOpponent;
    let score = isolatedScorePlayer - isolatedScoreOpponent;

    if (opponentMovesNext && missingCombinationsForOpponent === 1) {
      score = -1 * this.getNumberToWin() - 2;
    } else if (!opponentMovesNext && missingCombinationsForPlayer === 1) {
      score = 1 * this.getNumberToWin() + 2;
    }

    if (isolatedScorePlayer === this.getNumberToWin()) {
      score = this.getNumberToWin() + 3;
    } if (isolatedScoreOpponent === this.getNumberToWin()) {
      score = -1 * this.getNumberToWin() - 3;
    }

    return score;
  }
}
