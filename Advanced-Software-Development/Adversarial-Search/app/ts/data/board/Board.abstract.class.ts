import _ from 'lodash';
import Field from '../Field.class';
// eslint-disable-next-line import/no-cycle
import Player from '../Player.class';

export default abstract class Board {
  private numRows: number
  private numColumns: number
  private fields: Field[]
  private moves: number[]
  private winningCombinations: number[][][];
  private numberToWin: number

  constructor(numColumns: number, numRows: number, numberToWin: number) {
    this.numRows = numRows;
    this.numColumns = numColumns;
    this.numberToWin = numberToWin;
    this.fields = [];
    this.moves = [];
    this.buildBoard();
    this.winningCombinations = this.calculateWinningCombinations();
  }

  // *** PUBLIC FUNCTIONS ***

  public getFields(): Field[] {
    return this.fields;
  }

  public getAvailableFields(): Field[] {
    return this.getEmptyFields();
  }

  public getDefaultDepthForSimulation(): number {
    return this.getAvailableFields().length;
  }

  public isFull(): boolean {
    return _.filter(this.fields, (f) => f.getPlayer() === undefined).length === 0;
  }

  public getWinningCombinations(): number[][][] {
    return this.winningCombinations;
  }

  public getNumberOfColumns(): number {
    return this.numColumns;
  }

  public getMoves(): number[] {
    return this.moves;
  }

  public assignFieldToPlayer(field: Field, player: Player): void {
    const fieldToAssign = _.find(this.fields, field);
    if (fieldToAssign === undefined) {
      console.error();
    }
    fieldToAssign.assignToPlayer(player);
    this.moves.push(_.indexOf(this.fields, fieldToAssign) + 1);
  }

  public printBoardState(): void {
    let state = '';
    for (let i = this.numRows; i >= 1; i -= 1) {
      let str = '';
      for (let n = 1; n <= this.numColumns; n += 1) {
        const field = _.find(this.fields, { x: n, y: i }) as Field;
        str += `${field.getPlayer() === undefined ? '*' : field.getPlayer().getName()} - `;
      }
      state = `${state + str}\n`;
    }
    console.log(`${state}\n`);
  }

  // eslint-disable-next-line class-methods-use-this
  public evaluateBoardStateForPlayer(_player: Player, _opponent: Player, _isMaximising: boolean): number {
    return 0;
  }

  public calculateNumberOfPossibleSolutions(depth: number): number {
    const numberOfAvailableFields = this.getAvailableFields().length;
    return this.factorial(numberOfAvailableFields, 1, depth);
  }

  // eslint-disable-next-line class-methods-use-this
  public cloneBoard(): Board {
    throw new Error('Method not implemented!');
  }

  // eslint-disable-next-line class-methods-use-this
  public cloneEmptyBoard(): Board {
    throw new Error('Method not implemented!');
  }

  public getEmptyFields(): Field[] {
    return _.filter(this.fields, (f) => f.getPlayer() === undefined);
  }

  // *** PROTECTED FUNCTIONS ***

  protected getNumberToWin(): number {
    return this.numberToWin;
  }

  // *** PRIVATE FUNCTIONS ***

  private buildBoard(): void {
    for (let i = 1; i <= this.numRows; i += 1) {
      for (let n = 1; n <= this.numColumns; n += 1) {
        this.fields.push(new Field(n, i));
      }
    }
  }

  // eslint-disable-next-line class-methods-use-this
  private factorial(x: number, depth: number, maxDepth: number): number {
    if (x === 0) {
      return 1;
    }
    if (depth === maxDepth) {
      return x;
    }
    return x * this.factorial(x - 1, depth + 1, maxDepth);
  }

  private calculateWinningCombinations(): number[][][] {
    return _.concat(
      this.calculateHorizontalWinningCombinations(),
      this.calculateVerticalWinningCombinations(),
      this.calculateDiagonalWinningCombinations(),
    );
  }

  // Rows
  /**
    * Go through every row (horizontal) and add combination for every column with length of numberToWin
  */
  private calculateHorizontalWinningCombinations(): number[][][] {
    const wc = [];
    for (let y = 1; y <= this.numRows; y += 1) {
      for (let x = 1; x <= this.numColumns - this.numberToWin + 1; x += 1) {
        const c = [];
        for (let m = 0; m < this.numberToWin; m += 1) {
          c.push([x + m, y]);
        }
        wc.push(c);
      }
    }
    return wc;
  }

  // Columns
  /**
    * Go through every column (vertical) and add combination for every row with length of numberToWin
  */
  private calculateVerticalWinningCombinations(): number[][][] {
    const wc = [];
    for (let x = 1; x <= this.numColumns; x += 1) {
      for (let y = 1; y <= this.numRows - this.numberToWin + 1; y += 1) {
        const c = [];
        for (let m = 0; m < this.numberToWin; m += 1) {
          c.push([x, y + m]);
        }
        wc.push(c);
      }
    }
    return wc;
  }

  // Diagonals
  /**
    * Check for every field if a diagonal combination is possible
    *  -> possible directions are upRight, upLeft, downRight, downLeft
    * If the combination is possible then add coordinates to winning combinations array accordingly
    * When adding the combinations to the dwc array, sort them by their x value to remove duplicates later more easily
    * Remove duplicates of dwc array in the end -> upRight from field 1,1 is the same as downLeft from 3,3
  */
  private calculateDiagonalWinningCombinations(): number[][][] {
    const dwc = [];
    for (let x = 1; x <= this.numColumns; x += 1) {
      for (let y = 1; y <= this.numRows; y += 1) {
        /**
         * upRight -> ++
         * upLeft -> -+
         * downRight -> +-
         * downLeft -> --
       */
        const upRightPossible = x + this.numberToWin - 1 <= this.numColumns && y + this.numberToWin - 1 <= this.numRows;
        const upLeftPossible = x - this.numberToWin + 1 >= 1 && y + this.numberToWin - 1 <= this.numRows;
        const downRightPossible = x + this.numberToWin - 1 <= this.numColumns && y - this.numberToWin + 1 >= 1;
        const downLeftPossible = x - this.numberToWin + 1 >= 1 && y - this.numberToWin + 1 >= 1;

        if (upRightPossible) {
          const c = [];
          for (let m = 0; m < this.numberToWin; m += 1) {
            c.push([x + m, y + m]);
          }
          dwc.push(_.sortBy(c, [function (o) { return o[0]; }]));
        }

        if (upLeftPossible) {
          const c = [];
          for (let m = 0; m < this.numberToWin; m += 1) {
            c.push([x - m, y + m]);
          }
          dwc.push(_.sortBy(c, [function (o) { return o[0]; }]));
        }

        if (downRightPossible) {
          const c = [];
          for (let m = 0; m < this.numberToWin; m += 1) {
            c.push([x + m, y - m]);
          }
          dwc.push(_.sortBy(c, [function (o) { return o[0]; }]));
        }

        if (downLeftPossible) {
          const c = [];
          for (let m = 0; m < this.numberToWin; m += 1) {
            c.push([x - m, y - m]);
          }
          dwc.push(_.sortBy(c, [function (o) { return o[0]; }]));
        }
      }
    }
    return _.uniqWith(dwc, _.isEqual);
  }
}
