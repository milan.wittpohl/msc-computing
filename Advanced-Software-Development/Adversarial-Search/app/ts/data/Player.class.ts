// eslint-disable-next-line import/no-cycle
import EvaluationAlgorithmInterface from '../service/evaluationAlgorithms/EvaluationAlgorithm.interface';

export default class Player {
  private name: string;
  private evaluationAlgorithm: EvaluationAlgorithmInterface;

  constructor(name: string, evaluationAlgorithm: EvaluationAlgorithmInterface) {
    this.name = name;
    this.evaluationAlgorithm = evaluationAlgorithm;
  }

  // *** PUBLIC FUNCTIONS ***

  public getName(): string {
    return this.name;
  }

  public getEvaluationAlgorithm(): EvaluationAlgorithmInterface {
    return this.evaluationAlgorithm;
  }

  // *** PRIVATE FUNCTIONS ***
}
