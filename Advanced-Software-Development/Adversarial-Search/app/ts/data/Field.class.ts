// eslint-disable-next-line import/no-cycle
import Player from './Player.class';

export default class Field {
  private x: number
  private y: number
  private player: Player

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
    this.player = undefined;
  }

  // *** PUBLIC FUNCTIONS ***

  public isOccupied(): boolean {
    return this.player !== undefined;
  }

  public getPlayer(): Player {
    return this.player;
  }

  public getX(): number {
    return this.x;
  }

  public getY(): number {
    return this.y;
  }

  public getState(): string {
    if (this.isOccupied()) {
      return `${this.x} - ${this.y} - ${this.player.getName()}`;
    }
    return `${this.x} - ${this.y} - unassigned`;
  }

  public assignToPlayer(player: Player): void {
    this.player = player;
  }

  // *** PRIVATE FUNCTIONS ***
}
