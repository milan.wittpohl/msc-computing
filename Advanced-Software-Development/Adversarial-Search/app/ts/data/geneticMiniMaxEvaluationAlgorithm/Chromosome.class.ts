export default class Chromosome {
  private value: number[];
  private score: number;
  private fitness = 0;

  constructor(value: number[], score: number) {
    this.value = value;
    this.score = score;
  }

  public getValue(): number[] {
    return this.value;
  }

  public getScore(): number {
    return this.score;
  }

  public getFitness(): number {
    return this.fitness;
  }

  public setFitness(fitness: number): void {
    this.fitness = fitness;
  }
}
