import _ from 'lodash';
import GameService from './service/GameService';

const urlString = window.location.href;
const url = new URL(urlString);
const board = url.searchParams.get('board');
const numberOfGames = parseInt(url.searchParams.get('g'), 10);
const algorithmA = url.searchParams.get('a');
const algorithmB = url.searchParams.get('b');
console.log(board, numberOfGames, algorithmA, algorithmB);

let boardStr = 'Tic Tac Toe';
if (board === 'CONNECT_FOUR') {
  boardStr = 'Connect Four';
}

let algorithmAStr = 'Random';
if (algorithmA === 'MINIMAX') {
  algorithmAStr = 'Minimax';
} else if (algorithmA === 'ALPHA_BETA_PRUNING') {
  algorithmAStr = 'Alpha Beta Pruning';
} else if (algorithmA === 'GENETIC_MINIMAX') {
  algorithmAStr = 'Genetic Minimax';
}

let algorithmBStr = 'Random';
if (algorithmB === 'MINIMAX') {
  algorithmBStr = 'Minimax';
} else if (algorithmB === 'ALPHA_BETA_PRUNING') {
  algorithmBStr = 'Alpha Beta Pruning';
} else if (algorithmB === 'GENETIC_MINIMAX') {
  algorithmBStr = 'Genetic Minimax';
}

let gameService;
const simulationFacts = `Board: ${boardStr} - Number of Games: ${numberOfGames === 0 ? 1 : numberOfGames} <br /> Player A: ${algorithmAStr} - Player B: ${algorithmBStr}`;
document.getElementById('simulation-facts').innerHTML = simulationFacts;

const simulatingElement = document.getElementById('simulating-game');
const winners = [];
const avgsA = [];
const avgsB = [];
const avgMoves = [];

function stats(): void {
  const winsByA = _.filter(winners, (o) => o === 'A').length;
  const winsByB = _.filter(winners, (o) => o === 'B').length;
  const winsByU = _.filter(winners, (o) => o === undefined).length;
  const avgA = _.sum(avgsA) / numberOfGames;
  const avgB = _.sum(avgsB) / numberOfGames;
  const avgM = _.sum(avgMoves) / numberOfGames;
  console.log(`\nA: ${winsByA}\nB: ${winsByB}\nTie: ${winsByU}\nAvgA: ${avgA}\nAvgB: ${avgB}\n`);
  document.getElementById('board').innerHTML = boardStr;
  document.getElementById('number-of-games').innerHTML = numberOfGames.toString();
  document.getElementById('player-a').innerHTML = algorithmAStr;
  document.getElementById('player-b').innerHTML = algorithmBStr;
  document.getElementById('wins-a').innerHTML = winsByA.toString();
  document.getElementById('wins-b').innerHTML = winsByB.toString();
  document.getElementById('ties').innerHTML = winsByU.toString();
  document.getElementById('time-a').innerHTML = avgA.toString();
  document.getElementById('time-b').innerHTML = avgB.toString();
  document.getElementById('avg-moves').innerHTML = avgM.toString();
  document.getElementById('simulation-summary-container').classList.remove('hide');
}


function simulateGames(n: number): void {
  gameService = new GameService(board, algorithmA, algorithmB);
  const result = gameService.startgame();
  let winnerStr = `Winner: Player ${result[0]}`;
  if (result[0] === undefined) {
    winnerStr = 'Tie';
  }
  const simulationResultStr = `Completed Simulation of Game ${numberOfGames - n + 1} - ${winnerStr}<br />`;
  simulatingElement.innerHTML += simulationResultStr;
  winners.push(result[0]);
  avgsA.push(result[1][0]);
  avgsB.push(result[1][1]);
  avgMoves.push(result[1][2]);
  console.log(`Game: ${numberOfGames - n + 1} completed. Winner: ${result[0]}\n`);
  if (n > 1) {
    _.defer(simulateGames, n - 1);
  } else {
    _.defer(stats);
  }
}

function assignFieldToPlayer(field: number, player: string): void {
  let color = '#389CFF';
  color = player === 'A' ? color : '#F3553A';
  document.getElementById(`field-${field}`).innerHTML = `<span style="color:${color}">${player}</span>`;
}

declare global {
  interface Window {
    simulateMove: any;
    move: any;
  }
}

let player = 'B';
function makeMove(x: number, y: number): void {
  (document.getElementById('simulate-btn') as HTMLButtonElement).disabled = true;
  _.defer(() => {
    const lastmove = gameService.simulateMove(x, y);
    player = player === 'A' ? 'B' : 'A';
    assignFieldToPlayer(lastmove, player);
    const gameOver = gameService.gameIsOver();
    if (gameOver) {
      (document.getElementById('simulate-btn') as HTMLButtonElement).disabled = true;
    } else {
      (document.getElementById('simulate-btn') as HTMLButtonElement).disabled = false;
    }
  });
}

window.simulateMove = function simulateMove(): void {
  makeMove(undefined, undefined);
};

window.move = function move(x: number, y: number): void {
  makeMove(x, y);
};

function renderBoard(columns: number, rows: number): void {
  let html = '<div class="board">';
  const fieldNumber = columns * rows;
  for (let i = 0; i < rows; i += 1) {
    html += '<div class="board-row">';
    for (let n = columns - 1; n >= 0; n -= 1) {
      html += `<div class="field" id="field-${fieldNumber - n - (i * columns)}"></div>`;
    }
    html += '</div>';
  }
  html += '</div>';
  document.getElementById('board').innerHTML = html;
}

function buildBoard(): void {
  if (board === 'TIC_TAC_TOE') {
    renderBoard(3, 3);
  } else if (board === 'CONNECT_FOUR') {
    renderBoard(7, 6);
  }
}


if (numberOfGames > 0) {
  _.defer(simulateGames, numberOfGames);
} else {
  buildBoard();
  gameService = new GameService(board, algorithmA, algorithmB);
}
