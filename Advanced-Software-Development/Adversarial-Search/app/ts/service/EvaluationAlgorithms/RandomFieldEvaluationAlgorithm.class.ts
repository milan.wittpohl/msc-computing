import _ from 'lodash';
import EvaluationAlgorithmInterface from './EvaluationAlgorithm.interface';
import Board from '../../data/board/Board.abstract.class';
import Field from '../../data/Field.class';
import Player from '../../data/Player.class';

export default class RandomFieldEvaluationAlgorithm implements EvaluationAlgorithmInterface {
  // *** PUBLIC FUNCTIONS ***

  // eslint-disable-next-line class-methods-use-this
  public evaluateBoard(board: Board, player: Player, opponent: Player): Field {
    const emptyFields = board.getAvailableFields();
    const randomIndex = _.random(emptyFields.length - 1);
    return emptyFields[randomIndex];
  }
}
