import _ from 'lodash';
import EvaluationAlgorithmInterface from './EvaluationAlgorithm.interface';
import Board from '../../data/board/Board.abstract.class';
import Field from '../../data/Field.class';
import Player from '../../data/Player.class';
import EvaluationService from '../EvaluationService';
import TicTacToeBoard from '../../data/board/TicTacToeBoard.class';

export default class MiniMaxEvaluationAlgorithm implements EvaluationAlgorithmInterface {
  private debug = false;
  private extendedLogging = false;
  private maxDepthForDebug = 9;
  private numOfNodesChecked = 0;

  // *** PUBLIC FUNCTIONS ***

  // eslint-disable-next-line class-methods-use-this
  public evaluateBoard(board: Board, player: Player, opponent: Player): Field {
    let depth = this.debug ? this.maxDepthForDebug : board.getDefaultDepthForSimulation();
    if (this.maxDepthForDebug > board.getDefaultDepthForSimulation()) depth = board.getDefaultDepthForSimulation();
    this.numOfNodesChecked = 0;
    // inital call to minimax function - the player whos turn it is, is always maximising
    const bestScoreWithField = this.minimax(board, undefined, depth, true, player, opponent);
    if (this.debug) console.log(`Number of nodes checked by minimax: ${this.numOfNodesChecked}`);
    return bestScoreWithField[1];
  }

  // *** PRIVATE FUNCTIONS ***

  // eslint-disable-next-line class-methods-use-this
  /**
   * recursive function to calculate the maximum of all the minimum scores for the current player
   * When called the current player is always maximising and the opponent is always minimising
   * Within the function the setting of which player is the player and which is the opponent always stays the same
   */
  private minimax(board: Board, newField: Field, depth: number, isMaximising: boolean, player: Player, opponent: Player): [number, Field] {
    /**
     * The minimax function stops evaluating recursively if one of the following (game over) cases is true
     * -> the depth is 0
     *    -> No next move should be taken into account
     * -> the board is full
     *    -> as the board is full, no next move should be taken into account
     * -> the board has a winning player
     *
     * In those cases the function
     *    -> returns the score of the board for the current state
     *    -> of all the returned scores either the max or min is then taken
     *
     * -> The score depends on whos turn it is next, therefore isMaximising is passed along
     */
    if (depth === 0 || board.isFull() || EvaluationService.checkIfBoardStateHasWinningPlayer(board, player, opponent) !== undefined) {
      return [board.evaluateBoardStateForPlayer(player, opponent, isMaximising), newField];
    }

    // evaluation for the maximising player
    //  -> return the max values of either the next level or current level (depth = 0)
    if (isMaximising) {
      // default return value is the worst possible state
      // -> should never get returned,
      //    -> as any other new state should be better,
      //    -> or board is already full, then the upper if statement is executed
      let maxScoreAndBoard: [number, Field] = [-Infinity, undefined];
      // generate all possible gamestates, by adding player to every single available (empty) field
      const newFieldAndBoardsMax = this.generatePossibleNextBoards(board, player);
      const debugMax = [];
      // evaluate each possible game state and return the one with the highest score
      _.forEach(newFieldAndBoardsMax, (newFieldAndBoard) => {
        // the score is calculated recursively, by returning the minimal value of the next level
        // the field to return is not effected by the minimax call
        const scoreAndBoard: [number, Field] = [
          this.minimax(newFieldAndBoard[1], newFieldAndBoard[0], depth - 1, false, player, opponent)[0],
          newFieldAndBoard[0],
        ];

        this.numOfNodesChecked += 1;
        debugMax.push(scoreAndBoard);

        // select the largest of all the minimal scores
        if (scoreAndBoard[0] > maxScoreAndBoard[0]) {
          maxScoreAndBoard = scoreAndBoard;
        }
      });

      // verbose debug output
      if (this.debug) {
        let str = `\nScore (${depth},${player.getName()},${opponent.getName()}) - MAX:`;
        if (this.extendedLogging) {
          if (newField !== undefined) {
            str = `\nScore (${depth},${player.getName()},${opponent.getName()}) - MAX - ${newField.getX()},${newField.getY()}:`;
          }
        }
        _.forEach(debugMax, (max) => {
          if (max[0] === maxScoreAndBoard[0]) {
            str = `${str} | ${max[0]}!`;
          } else {
            str = `${str} | ${max[0]}`;
          }
        });
        if (this.extendedLogging || depth === this.maxDepthForDebug) {
          console.log(str);
        }
      }

      // return, therefore no else is needed for minimising
      return maxScoreAndBoard;
    }

    // evaluate for the minimising player

    // default return value is the worst possible state
    // -> should never get returned,
    //    -> as any other new state should be better,
    //    -> or board is already full, then the upper if statement is executed
    let minScoreAndBoard: [number, Field] = [Infinity, undefined];
    // generate all possible gamestates, by adding the opponent to every single available (empty) field
    const newFieldAndBoardsMin = this.generatePossibleNextBoards(board, opponent);
    const debugMin = [];
    // evaluate each possible game state and return the one with the lowest score
    _.forEach(newFieldAndBoardsMin, (newFieldAndBoard) => {
      // the score is calculated recursively, by returning the maximal value of the next level
      // the field to return is not effected by the minimax call
      const scoreAndBoard: [number, Field] = [
        this.minimax(newFieldAndBoard[1], newFieldAndBoard[0], depth - 1, true, player, opponent)[0],
        newFieldAndBoard[0],
      ];

      this.numOfNodesChecked += 1;
      debugMin.push(scoreAndBoard);

      // select the smallest of all the maximal scores
      if (scoreAndBoard[0] < minScoreAndBoard[0]) {
        minScoreAndBoard = scoreAndBoard;
      }
    });

    // verbose debug output
    if (this.debug && this.extendedLogging) {
      let str = `\nScore (${depth},${player.getName()},${opponent.getName()}) - MIN - ${newField.getX()},${newField.getY()}:`;
      _.forEach(debugMin, (min) => {
        if (min[0] === minScoreAndBoard[0]) {
          str = `${str} | ${min[0]}!`;
        } else {
          str = `${str} | ${min[0]}`;
        }
      });
      console.log(str);
    }

    return minScoreAndBoard;
  }

  // eslint-disable-next-line class-methods-use-this
  private generatePossibleNextBoards(board: Board, player: Player): [[Field, Board]] {
    const newBoards = [] as unknown as [[Field, Board]];
    _.forEach(board.getAvailableFields(), (field) => {
      const newBoard = board.cloneBoard();
      newBoard.assignFieldToPlayer(field, player);
      newBoards.push([field, newBoard]);
    });
    return newBoards;
  }
}
