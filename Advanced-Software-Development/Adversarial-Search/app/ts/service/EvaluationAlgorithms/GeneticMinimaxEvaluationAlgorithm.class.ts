/* eslint-disable class-methods-use-this */
import _ from 'lodash';
import EvaluationAlgorithmInterface from './EvaluationAlgorithm.interface';
import Board from '../../data/board/Board.abstract.class';
import Player from '../../data/Player.class';
import Field from '../../data/Field.class';
import Chromosome from '../../data/geneticMiniMaxEvaluationAlgorithm/Chromosome.class';
import EvaluationService from '../EvaluationService';
import ConnectFourBoard from '../../data/board/ConnectFourBoard.class';

export default class GeneticMinimaxEvaluationAlgorithm implements EvaluationAlgorithmInterface {
  private populationSize: number;
  private maxPopulationSize: number;
  private population: Chromosome[];
  private depth: number;
  private startTimeStamp: Date;
  private maxDurationInMilliseconds: number;
  private numIterations = 0;

  // *** PUBLIC FUNCTIONS ***

  evaluateBoard(board: Board, player: Player, opponent: Player, playerA: Player, playerB: Player): Field {
    this.reset(board.getDefaultDepthForSimulation(), board instanceof ConnectFourBoard ? 'CONNECT_FOUR' : 'TIC_TAC_TOE');
    // calculate the population size based on the number of possible moves, limited via function
    this.populationSize = this.getInitalPopulationSize(board.calculateNumberOfPossibleSolutions(this.depth));
    // create a random population to start with - playerA is the first player to move, not the current player
    this.createRandomPopulation(board, playerA, playerB, player, opponent);
    let elapsedTime = (new Date()).getTime() - this.startTimeStamp.getTime();
    // progress the population
    this.numIterations = 0;
    while (elapsedTime <= this.maxDurationInMilliseconds) {
      this.progressPopulation(board, playerA, playerB, player, opponent);
      this.numIterations += 1;
      elapsedTime = (new Date()).getTime() - this.startTimeStamp.getTime();
    }
    // select best chromosome from population
    const bestChromosome = this.getBestChromosome();
    // select the next move from best chromosome
    const bestField = this.decodeNextMoveFromChromosome(
      bestChromosome.getValue(),
      board.getMoves().length,
    );
    this.printStats();
    // if bestField is 0, then the player lost in all cases, just take the first free field then
    if (bestField === 0) {
      return _.head(board.getAvailableFields());
    }
    // return the field by decoding the value from the chromosome
    const field = this.decodeField(bestField, board.getNumberOfColumns());
    return field;
  }

  // *** PRIVATE FUNCTIONS ***

  private reset(boardDefaultDepth: number, boardType: string): void {
    let a = 300;
    let b = 10;
    let c = 1000;
    if (boardType === 'CONNECT_FOUR') {
      a = 100;
      b = 10;
      c = 1000;
    }

    this.population = [];
    this.startTimeStamp = new Date();
    this.populationSize = undefined;
    // this.maxPopulationSize = Math.ceil(this.factorial(boardDefaultDepth));
    // this.maxPopulationSize = this.maxPopulationSize > a ? a : this.maxPopulationSize;
    this.maxPopulationSize = a;
    this.maxPopulationSize = this.maxPopulationSize < b ? b : this.maxPopulationSize;
    this.maxDurationInMilliseconds = c;
    this.depth = boardDefaultDepth;
  }

  private printStats(): void {
    console.log('Population Size: ', this.populationSize);
    console.log('Depth: ', this.depth);
    console.log('Elapsed Time: ', (new Date()).getTime() - this.startTimeStamp.getTime());
    console.log('Iterations: ', this.numIterations);
  }

  // eslint-disable-next-line class-methods-use-this
  private factorial(x: number): number {
    if (x === 0) {
      return 1;
    }
    return x * this.factorial(x - 1);
  }

  private getInitalPopulationSize(numberPossibleSolutions: number): number {
    return numberPossibleSolutions > this.maxPopulationSize ? this.maxPopulationSize : numberPossibleSolutions;
  }

  /**
   * Create random population by:
   *  -> converting available fields to their number representation
   *  -> creates a chromosome
   *    -> add already added fields
   *    -> shuffle available fields
   *    -> build chromosome value array
   *    -> after adding one value, check if board is full or has winner
   *    -> if so, close chromosome by just adding 0s
   *  -> if chromosome is already present, dont add and try again, tries max of 100 times
   */
  private createRandomPopulation(board: Board, playerA: Player, playerB: Player, player: Player, opponent: Player): void {
    const availableFieldsEncoded = this.encodeFields(board.getAvailableFields(), board.getNumberOfColumns());
    let duplicates = 0;
    for (let i = 0; i < this.populationSize; i += 1) {
      const chromosomeValue = [];
      const availableFieldsShuffeled = _.shuffle(availableFieldsEncoded);
      _.forEach(board.getMoves(), (move) => {
        chromosomeValue.push(move);
      });

      let closeChromsome = false;
      // const clone = board.cloneBoard();
      // const upperBound = this.depth > board.getEmptyFields().length ? board.getEmptyFields().length : this.depth;
      // for (let n = 0; n < upperBound; n += 1) {
      for (let n = 0; n < this.depth; n += 1) {
        if (closeChromsome) {
          chromosomeValue.push(0);
        } else {
          // const availableFieldsEncoded = this.encodeFields(clone.getAvailableFields(), clone.getNumberOfColumns());
          // const availableFieldsShuffeled = _.shuffle(availableFieldsEncoded);
          // const availableFields = _.difference(availableFieldsShuffeled, chromosomeValue);
          // chromosomeValue.push(_.head(availableFields));
          chromosomeValue.push(availableFieldsShuffeled[n]);
          // board.assignFieldToPlayer(this.decodeField(availableFieldsShuffeled[n], clone.getNumberOfColumns()), n % 2 === 0 ? player : opponent);
          if (this.checkIfChromosomeContainsGameEndingState(
            board,
            playerA,
            playerB,
            new Chromosome(chromosomeValue, undefined),
          )) {
            closeChromsome = true;
          }
        }
      }
      // if duplicate skip
      if (_.find(this.population, (c) => _.isEqual(c.getValue(), chromosomeValue)) && duplicates < 100) {
        i -= 1;
        duplicates += 1;
      } else {
        const chromosome = new Chromosome(
          chromosomeValue,
          this.calculateChromosomesScore(
            board,
            chromosomeValue,
            playerA,
            playerB,
            player,
            opponent,
          ),
        );
        this.validateChromosomeValue(chromosome.getValue(), board.getFields().length);
        this.population.push(chromosome);
        duplicates = 0;
      }
    }
    this.calculatePopulationFitness();
  }

  /**
   * encode fields by converting their position to number
   * Examples for tic tac toe board:
   *  -> [1,1] -> 1 + ((1 - 1) * 3) = 1
   *  -> [3,1] -> 3 + ((1 - 1) * 3) = 3
   *  -> [1,2] -> 1 + ((2 - 1) * 3) = 4
   *  -> [3,3] -> 3 + ((3 - 1) * 3) = 9
   */
  private encodeFields(fields: Field[], numberOfColumns: number): number[] {
    const fieldsEncoded = [];
    _.forEach(fields, (field) => {
      fieldsEncoded.push(this.encodeField(field, numberOfColumns));
    });
    return fieldsEncoded;
  }

  private encodeField(field: Field, numberOfColumns: number): number {
    const value = field.getX() + ((field.getY() - 1) * numberOfColumns);
    return value;
  }

  /**
   * decode field
   * Examples for tic tac toe board:
   *  -> 1
   *    -> 1 % 3 = 1
   *    -> y = ceil(1 / 3) + 1 = 1
   *    -> x = 1 % 3 = 1
   *    -> [1,1]
   *  -> 3
   *    -> 3 % 3 = 0
   *    -> y = 3 / 3 = 1
   *    -> x = 3
   *    -> [3,1]
   *  -> 4
   *    -> 4 % 3 = 1
   *    -> y = ceil(4 / 3) + 1 = 2
   *    -> x = 4 % 3 = 1
   *    -> [1,2]
   *  -> 9
   *    -> 9 % 3 = 0
   *    -> y = 9 / 3 = 3
   *    -> x = 3
   *    -> [3,3]
   */
  private decodeField(value: number, numberOfColumns: number): Field {
    // if it is the last field in one row
    if (value % numberOfColumns === 0) {
      const y = value / numberOfColumns;
      const x = numberOfColumns;
      return new Field(x, y);
    }
    const y = Math.floor(value / numberOfColumns) + 1;
    const x = value % numberOfColumns;
    return new Field(x, y);
  }

  /**
   * Decode chromosome
   * Update board
   * Check if and return
   *  -> board is full
   *  -> board has winning player
   */
  private checkIfChromosomeContainsGameEndingState(board: Board, playerA: Player, playerB: Player, chromosome: Chromosome): boolean {
    const clone = board.cloneEmptyBoard();
    let pA = playerA;
    let pB = playerB;
    for (let i = 0; i < chromosome.getValue().length; i += 1) {
      const nextMove = this.decodeNextMoveFromChromosome(chromosome.getValue(), i);
      const field = this.decodeField(nextMove, board.getNumberOfColumns());
      clone.assignFieldToPlayer(
        field,
        pA,
      );
      const t = pA;
      pA = pB;
      pB = t;
    }
    const boardHasWinner = EvaluationService.checkIfBoardStateHasWinningPlayer(clone, pA, pB) !== undefined;
    return clone.isFull() || boardHasWinner;
  }

  /**
   * Calculate the chromosomes score (isolated score)
   * For each new number
   *  -> add to board
   * then calculate score
   * the current player is always maximising, therefore it is always true
   */
  private calculateChromosomesScore(
    board: Board, chromosomeValue: number[], playerA: Player, playerB: Player, player: Player, opponent: Player,
  ): number {
    let endIndex = _.indexOf(chromosomeValue, 0);
    if (endIndex === -1) { endIndex = chromosomeValue.length; }
    const clone = board.cloneEmptyBoard();
    let previousPlayer = playerB;
    let currentPlayer = playerA;
    for (let i = 0; i < endIndex; i += 1) {
      const nextMove = this.decodeNextMoveFromChromosome(chromosomeValue, i);
      const field = this.decodeField(nextMove, board.getNumberOfColumns());
      clone.assignFieldToPlayer(
        field,
        currentPlayer,
      );
      const t = previousPlayer;
      previousPlayer = currentPlayer;
      currentPlayer = t;
    }
    const opponentMovesNext = currentPlayer.getName() === opponent.getName();
    return clone.evaluateBoardStateForPlayer(player, opponent, opponentMovesNext);
  }

  /**
   * Build reservation tree
   * Put population into groups with same prefix
   * Calculate best (depending on maximising or not) chromosome
   * Repeat with all best chromosomes, now one less prefix
   */
  private calculatePopulationFitness(): void {
    // give me the chromosome with the highest value if the current player moves last
    const isMaximising = this.depth % 2 === 1;
    this.calculateFitnessForSubPopulation(this.population, 1, isMaximising);
  }

  private calculateFitnessForSubPopulation(chromosomes: Chromosome[], level: number, isMaximising: boolean): void {
    const firstNChromosomeValues = [];
    const n = _.head(chromosomes).getValue().length - level;
    if (level < this.depth + 1) {
      _.forEach(chromosomes, (c) => {
        const subValue = _.take(c.getValue(), n);
        firstNChromosomeValues.push(subValue);
      });
      const groups = _.uniqWith(firstNChromosomeValues, _.isEqual);
      const bestChromosomePerGroup = [];
      _.forEach(groups, (g) => {
        const bestChromosome = this.calculateBestChromosomeValueOfGroup(chromosomes, g, isMaximising);
        bestChromosome.setFitness(bestChromosome.getFitness() + 1);
        bestChromosomePerGroup.push(bestChromosome);
      });
      this.calculateFitnessForSubPopulation(bestChromosomePerGroup, level + 1, !isMaximising);
    }
  }

  private calculateBestChromosomeValueOfGroup(chromosomes: Chromosome[], group: number[], isMaximising: boolean): Chromosome {
    const relevantChromsomes = _.filter(chromosomes, (c) => {
      let relevant = true;
      _.forEach(group, (value, index) => {
        relevant = (value === c.getValue()[index]);
        // if relevant is false, stop foreach loop
        if (!relevant) { return false; }
        return true;
      });
      return relevant;
    });
    if (isMaximising) {
      return _.maxBy(relevantChromsomes, (c) => c.getScore());
    }
    return _.minBy(relevantChromsomes, (c) => c.getScore());
  }

  private progressPopulation(board: Board, playerA: Player, playerB: Player, player: Player, opponent: Player): void {
    const children: Chromosome[] = [];
    let numberOfChildern = Math.round(this.population.length / 10);
    numberOfChildern = numberOfChildern < 2 ? 1 : numberOfChildern;
    for (let i = 0; i < numberOfChildern; i += 1) {
      children.push(this.makeChild(board, playerA, playerB, player, opponent));
    }
    this.population = _.concat(this.population, children);
    this.calculatePopulationFitness();
    this.cleanPopulation();
  }

  /**
   * select 1/10 parents from population
   * select best two
   * apply two point crossover
   * apply one point swap
   */
  private makeChild(board: Board, playerA: Player, playerB: Player, player: Player, opponent: Player): Chromosome {
    // select randomly potential parents
    let potentialParents: Chromosome[] = [];
    const populationShuffeled = _.shuffle(this.population);
    let tournamentSize = Math.ceil(this.population.length / 10);
    tournamentSize = tournamentSize < 10 ? 10 : tournamentSize;
    tournamentSize = tournamentSize > this.population.length ? this.population.length : tournamentSize;
    if (tournamentSize === 1) {
      return _.head(this.population);
    }
    for (let i = 0; i < tournamentSize; i += 1) {
      potentialParents.push(populationShuffeled[i]);
    }
    // select best two
    potentialParents = _.orderBy(potentialParents, (c) => c.getFitness(), ['desc']);
    const bestParent = potentialParents[0];
    const secondBestParent = potentialParents[1];

    // if one parent has game ending state, just take the better one
    const bestParentHasGameEndingState = _.indexOf(bestParent.getValue(), 0) !== -1 && bestParent.getScore() > 0;
    const secondBestParentHasGameEndingState = _.indexOf(secondBestParent.getValue(), 0) !== -1 && secondBestParent.getScore() > 0;
    if (bestParentHasGameEndingState || secondBestParentHasGameEndingState) {
      return bestParent;
    }

    let endIndexBestParent = bestParent.getValue().length - 1;
    const indexOfZeroInBestParent = _.indexOf(bestParent.getValue(), 0);
    if (indexOfZeroInBestParent !== -1) {
      endIndexBestParent = indexOfZeroInBestParent - 1;
    }

    let endIndexSecondBestParent = secondBestParent.getValue().length - 1;
    const indexOfZeroInSecondBestParent = _.indexOf(secondBestParent.getValue(), 0);
    if (indexOfZeroInSecondBestParent !== -1) {
      endIndexSecondBestParent = indexOfZeroInSecondBestParent - 1;
    }

    const currentMoveIndex = board.getMoves().length;

    // otherwise get random index between next move index and end index -> crossover
    const randomIndex = _.random(currentMoveIndex, endIndexBestParent);
    const childsValue = bestParent.getValue();

    for (let i = randomIndex; i < endIndexBestParent && i < endIndexSecondBestParent; i += 1) {
      // we can not just take the value of second parent as we might get duplicates
      // we need to look for the index of that value in the best parent
      // if the value exsists then swap within the best parent
      // if the value does not exsist, just put at i
      const value = secondBestParent.getValue()[i];
      const indexOfValueInBestParent = _.indexOf(bestParent.getValue(), value);
      if (indexOfValueInBestParent !== -1) {
        const previousValueAtIndex = childsValue[i];
        childsValue[i] = value;
        childsValue[indexOfValueInBestParent] = previousValueAtIndex;
      } else {
        childsValue[i] = value;
      }
    }

    let endIndexChild = childsValue.length - 1;
    const indexOfZeroInChild = _.indexOf(childsValue, 0);
    if (indexOfZeroInChild !== -1) {
      endIndexChild = indexOfZeroInChild - 1;
    }

    // swap to random values in child
    const randomSwapOne = _.random(currentMoveIndex, endIndexChild);
    const randomSwapTwo = _.random(currentMoveIndex, endIndexChild);
    const t = childsValue[randomSwapOne];
    childsValue[randomSwapOne] = childsValue[randomSwapTwo];
    childsValue[randomSwapTwo] = t;

    this.validateChromosomeValue(childsValue, board.getFields().length);
    const score = this.calculateChromosomesScore(board, childsValue, playerA, playerB, player, opponent);
    const child = new Chromosome(childsValue, score);
    // console.log('new Child', child, 'parentA', bestParent, 'parentB', secondBestParent);
    return child;
  }

  private validateChromosomeValue(value: number[], numberOfFields: number): void {
    // if chromosome value contains undefined, fail
    _.forEach(value, (n) => {
      if (n === undefined) {
        throw new Error('Chromosome value contains undefined');
      }
    });

    // if chromosome value contains 0 but then a different number, fail
    const indexOfMinusOne = _.indexOf(value, 0);
    if (indexOfMinusOne !== -1) {
      for (let i = indexOfMinusOne; i < value.length; i += 1) {
        if (value[i] !== 0) {
          throw new Error(`Chromosome has positive values after 0 - values: ${value}`);
        }
      }
    }

    // if chromosome value contains duplicates, fail
    if (indexOfMinusOne === -1) {
      const uniqValues = _.uniq(value);
      if (uniqValues.length !== value.length) {
        throw new Error(`Chromosome has duplicates in value - values: ${value}`);
      }
    }

    // if chromosome is longer than number of fields, fail
    if (value.length > numberOfFields) {
      throw new Error(`Chromosome is too long - values: ${value}`);
    }
  }

  /**
   * New population are the best n chromosomes
   *  -> Order population by fitness desc
   *  -> select first n (depending on population size)
   */
  private cleanPopulation(): void {
    this.population = _.slice(_.orderBy(this.population, (c) => c.getFitness(), ['desc']), 0, this.populationSize);
  }

  /**
   * Return best chromsome by ordering by fitness desc and randomly selecting one of the best
   */
  private getBestChromosome(): Chromosome {
    const bestChromosome = _.maxBy(this.population, (c) => c.getFitness());
    const bestChromosomes = _.filter(this.population, (c) => c.getFitness() === bestChromosome.getFitness());
    return bestChromosomes[_.random(0, bestChromosomes.length - 1)];
  }


  /**
   * get next move from chromosome
   */
  private decodeNextMoveFromChromosome(chromosomeValue: number[], index: number): number {
    return chromosomeValue[index];
  }
}
