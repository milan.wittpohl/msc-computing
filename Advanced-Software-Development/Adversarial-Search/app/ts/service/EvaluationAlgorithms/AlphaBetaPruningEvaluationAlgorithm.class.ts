import _ from 'lodash';
import EvaluationAlgorithmInterface from './EvaluationAlgorithm.interface';
import Board from '../../data/board/Board.abstract.class';
import Field from '../../data/Field.class';
import Player from '../../data/Player.class';
import EvaluationService from '../EvaluationService';

export default class AlphaBetaPruningEvaluationAlgorithm implements EvaluationAlgorithmInterface {
  private debug = false;
  private extendedLogging = false;
  private maxDepthForDebug = 3;
  private numOfNodesChecked = 0;

  // *** PUBLIC FUNCTIONS ***

  // eslint-disable-next-line class-methods-use-this
  public evaluateBoard(board: Board, player: Player, opponent: Player): Field {
    let depth = this.debug ? this.maxDepthForDebug : board.getDefaultDepthForSimulation();
    if (this.maxDepthForDebug > board.getDefaultDepthForSimulation()) depth = board.getDefaultDepthForSimulation();
    this.numOfNodesChecked = 0;
    // inital call to minimax function - the player whos turn it is, is always maximising
    const bestScoreWithField = this.alphaBetaPruning(board, undefined, depth, true, player, opponent, -Infinity, Infinity);
    if (this.debug) console.log(`Number of nodes checked by alpha-beta: ${this.numOfNodesChecked}`);
    return bestScoreWithField[1];
  }

  // *** PRIVATE FUNCTIONS ***

  // eslint-disable-next-line class-methods-use-this
  /**
   * Extention of the minimax algorithm by pruning subtrees if there are not worth exploring
   * See inline comments of minimax to better understand the algorithm in general
   * Differences to minimax are commented below
   *
   * This function also takes in a value for alpha and beta
   *  -> alpha is the best score for the maximising player at the current depth
   *  -> beta is the best score for the minimising player at the current depth
   */
  private alphaBetaPruning(
    board: Board, newField: Field, depth: number, isMaximising: boolean, player: Player, opponent: Player, newAlpha: number, newBeta: number,
  ):
    [number, Field] {
    let alpha = newAlpha;
    let beta = newBeta;

    if (depth === 0 || board.isFull() || EvaluationService.checkIfBoardStateHasWinningPlayer(board, player, opponent) !== undefined) {
      return [board.evaluateBoardStateForPlayer(player, opponent, isMaximising), newField];
    }

    // evaluation for the maximising player
    //  -> return the max values of either the next level or current level (depth = 0)
    if (isMaximising) {
      // default return value is the worst possible state
      // -> should never get returned,
      //    -> as any other new state should be better,
      //    -> or board is already full, then the upper if statement is executed
      let maxScoreAndBoard: [number, Field] = [-Infinity, undefined];
      const newFieldAndBoardsMax = this.generatePossibleNextBoards(board, player);
      const debugMax = [];
      _.forEach(newFieldAndBoardsMax, (newFieldAndBoard) => {
        const scoreAndBoard: [number, Field] = [
          this.alphaBetaPruning(newFieldAndBoard[1], newFieldAndBoard[0], depth - 1, false, player, opponent, alpha, beta)[0],
          newFieldAndBoard[0],
        ];

        this.numOfNodesChecked += 1;
        debugMax.push(scoreAndBoard);

        // select the largest of all the minimal scores
        if (scoreAndBoard[0] > maxScoreAndBoard[0]) {
          maxScoreAndBoard = scoreAndBoard;
        }

        // alpha is the best score of the currently evaluated subtree
        // if beta, the best score for the minimising player of the previous level (depth), is smaler,
        // then don't explore the tree anymore, as the oponnent will never chose this subtree
        alpha = Math.max(alpha, maxScoreAndBoard[0]);
        if (beta <= alpha) {
          return false;
        }
      });

      // verbose debug output
      if (this.debug) {
        let str = `\nScore (${depth},${player.getName()},${opponent.getName()}) - MAX:`;
        if (this.extendedLogging) {
          if (newField !== undefined) {
            str = `\nScore (${depth},${player.getName()},${opponent.getName()}) - MAX - ${newField.getX()},${newField.getY()}:`;
          }
        }
        _.forEach(debugMax, (max) => {
          if (max[0] === maxScoreAndBoard[0]) {
            str = `${str} | ${max[0]}!`;
          } else {
            str = `${str} | ${max[0]}`;
          }
        });
        if (this.extendedLogging || depth === this.maxDepthForDebug) {
          console.log(str);
        }
      }

      // return, therefore no else is needed for minimising
      return maxScoreAndBoard;
    }

    // evaluate for the minimising player

    // default return value is the worst possible state
    // -> should never get returned,
    //    -> as any other new state should be better,
    //    -> or board is already full, then the upper if statement is executed
    let minScoreAndBoard: [number, Field] = [Infinity, undefined];
    // generate all possible gamestates, by adding the opponent to every single available (empty) field
    const newFieldAndBoardsMin = this.generatePossibleNextBoards(board, opponent);
    const debugMin = [];
    // evaluate each possible game state and return the one with the lowest score
    _.forEach(newFieldAndBoardsMin, (newFieldAndBoard) => {
      // the score is calculated recursively, by returning the maximal value of the next level
      // the field to return is not effected by the minimax call
      const scoreAndBoard: [number, Field] = [
        this.alphaBetaPruning(newFieldAndBoard[1], newFieldAndBoard[0], depth - 1, true, player, opponent, alpha, beta)[0],
        newFieldAndBoard[0],
      ];

      this.numOfNodesChecked += 1;
      debugMin.push(scoreAndBoard);

      // select the smallest of all the maximal scores
      if (scoreAndBoard[0] < minScoreAndBoard[0]) {
        minScoreAndBoard = scoreAndBoard;
      }

      // beta is the best score of the currently evaluated subtree
      // if alpha, the best score for the maximising player of the previous level (depth), is greater,
      // then don't explore the tree anymore, as the oponnent will never chose this subtree
      beta = Math.min(beta, minScoreAndBoard[0]);
      if (beta <= alpha) {
        return false;
      }
    });

    // verbose debug output
    if (this.debug && this.extendedLogging) {
      let str = `\nScore (${depth},${player.getName()},${opponent.getName()}) - MIN - ${newField.getX()},${newField.getY()}:`;
      _.forEach(debugMin, (min) => {
        if (min[0] === minScoreAndBoard[0]) {
          str = `${str} | ${min[0]}!`;
        } else {
          str = `${str} | ${min[0]}`;
        }
      });
      console.log(str);
    }

    return minScoreAndBoard;
  }

  // eslint-disable-next-line class-methods-use-this
  private generatePossibleNextBoards(board: Board, player: Player): [[Field, Board]] {
    const newBoards = [] as unknown as [[Field, Board]];
    _.forEach(board.getAvailableFields(), (field) => {
      const newBoard = board.cloneBoard();
      newBoard.assignFieldToPlayer(field, player);
      newBoards.push([field, newBoard]);
    });
    return newBoards;
  }
}
