import Field from '../../data/Field.class';
// eslint-disable-next-line import/no-cycle
import Board from '../../data/board/Board.abstract.class';
// eslint-disable-next-line import/no-cycle
import Player from '../../data/Player.class';

export default interface EvaluationAlgorithmInterface {
  evaluateBoard(board: Board, currentPlayer: Player, currentOpponent: Player, playerA: Player, playerB: Player): Field;
};
