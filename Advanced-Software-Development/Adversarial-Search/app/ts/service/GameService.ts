import _ from 'lodash';
import Board from '../data/board/Board.abstract.class';
import Player from '../data/Player.class';
import EvaluationService from './EvaluationService';
import Field from '../data/Field.class';
import TicTacToeBoard from '../data/board/TicTacToeBoard.class';
import RandomFieldEvaluationAlgorithm from './evaluationAlgorithms/RandomFieldEvaluationAlgorithm.class';
import MiniMaxEvaluationAlgorithm from './evaluationAlgorithms/MiniMaxEvaluationAlgorithm.class';
import AlphaBetaPruningEvaluationAlgorithm from './evaluationAlgorithms/AlphaBetaPruningEvaluationAlgorithm.class';
import GeneticMinimaxEvaluationAlgorithm from './evaluationAlgorithms/GeneticMinimaxEvaluationAlgorithm.class';
import ConnectFourBoard from '../data/board/ConnectFourBoard.class';
import EvaluationAlgorithmInterface from './evaluationAlgorithms/EvaluationAlgorithm.interface';

export default class GameService {
  private board: Board
  private players: [Player, Player]
  private durations: [number[], number[]];
  private currentPlayerIndex = -1;
  private winner: string = undefined;

  constructor(boardStr: string, playerAStr: string, playerBStr: string) {
    if (boardStr === 'TIC_TAC_TOE') {
      this.board = new TicTacToeBoard();
    } else if (boardStr === 'CONNECT_FOUR') {
      this.board = new ConnectFourBoard();
    }
    const playerA = new Player('A', this.getEvaluationAlgorithm(playerAStr));
    const playerB = new Player('B', this.getEvaluationAlgorithm(playerBStr));
    this.players = [playerA, playerB];
    this.durations = [[], []];
  }

  // *** PUBLIC FUNCTIONS ***

  public startgame(): [string, [number, number, number]] {
    while (!this.gameIsOver()) {
      this.simulateMove(undefined, undefined);
    }
    return this.getResults();
  }

  public simulateMove(x: number, y: number): number {
    this.currentPlayerIndex = this.nextPlayerIndex();
    // Player is always the current player
    // Opponent is always the other player
    const startTime = (new Date()).getTime();
    let nextMove: Field;
    if (x !== undefined && y !== undefined) {
      nextMove = new Field(x, y);
    } else {
      nextMove = EvaluationService.evaluateNextMoveForPlayer(
        this.board,
        this.players[this.currentPlayerIndex],
        this.players[this.nextPlayerIndex()],
        this.players[0],
        this.players[1],
      );
    }
    const endTime = (new Date()).getTime();
    const duration = endTime - startTime;
    this.durations[this.currentPlayerIndex].push(duration);
    this.board.assignFieldToPlayer(nextMove, this.players[this.currentPlayerIndex]);
    // this.board.printBoardState();
    this.winner = EvaluationService.checkIfBoardStateHasWinningPlayer(this.board, this.players[0], this.players[1]);
    return _.last(this.board.getMoves());
  }

  public gameIsOver(): boolean {
    return this.winner !== undefined || this.board.isFull();
  }

  public getResults(): [string, [number, number, number]] {
    const avgs = this.endGame();
    return [this.winner, avgs];
  }

  // *** PRIVATE FUNCTIONS ***

  // eslint-disable-next-line class-methods-use-this
  private getEvaluationAlgorithm(str: string): EvaluationAlgorithmInterface {
    if (str === 'MINIMAX') {
      return new MiniMaxEvaluationAlgorithm();
    } if (str === 'ALPHA_BETA_PRUNING') {
      return new AlphaBetaPruningEvaluationAlgorithm();
    } if (str === 'GENETIC_MINIMAX') {
      return new GeneticMinimaxEvaluationAlgorithm();
    }
    return new RandomFieldEvaluationAlgorithm();
  }

  private nextPlayerIndex(): number {
    if (this.currentPlayerIndex === -1 || this.currentPlayerIndex === 1) {
      return 0;
    }
    return 1;
  }

  private endGame(): [number, number, number] {
    // this.board.printBoardState();
    const avgTimePlayerA = _.sum(this.durations[0]) / this.durations[0].length;
    const avgTimePlayerB = _.sum(this.durations[1]) / this.durations[1].length;
    // console.log('Avg Time Player A:', avgTimePlayerA);
    // console.log('Avg Time Player B:', avgTimePlayerB);
    return [avgTimePlayerA, avgTimePlayerB, this.board.getMoves().length / 2];
  }
}
