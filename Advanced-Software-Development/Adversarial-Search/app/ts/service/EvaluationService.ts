import _ from 'lodash';
import Board from '../data/board/Board.abstract.class';
import Player from '../data/Player.class';
import Field from '../data/Field.class';

export default class EvaluationService {
  // *** PUBLIC FUNCTIONS ***

  public static evaluateNextMoveForPlayer(board: Board, player: Player, opponent: Player, playerA: Player, playerB: Player): Field {
    return player.getEvaluationAlgorithm().evaluateBoard(board, player, opponent, playerA, playerB);
  }

  public static checkIfBoardStateHasWinningPlayer(board: Board, playerA: Player, playerB: Player): string {
    let winner;
    const missingCombinationsPlayerA = this.calculateMissingWinningCombinations(board, playerA, playerB);
    const missingCombinationsPlayerB = this.calculateMissingWinningCombinations(board, playerB, playerA);
    if (missingCombinationsPlayerA === 0) {
      winner = playerA.getName();
    } else if (missingCombinationsPlayerB === 0) {
      winner = playerB.getName();
    }
    return winner;
  }

  public static calculateMissingWinningCombinationsForPlayer(board: Board, player: Player, opponent: Player): number {
    return this.calculateMissingWinningCombinations(board, player, opponent);
  }

  // *** PRIVATE FUNCTIONS ***

  /**
   * Returns the number of missing combintions to win -> 0 means win
   * @param board
   * @param fields
   */
  private static calculateMissingWinningCombinations(board: Board, player: Player, opponent: Player): number {
    const winningCombinations = board.getWinningCombinations();
    const missingWinningCombinationsArray = [];
    _.forEach(winningCombinations, (wc) => {
      let missingWinningCombinations = wc.length;
      _.forEach(wc, (c) => {
        const fieldOnBoard = _.find(board.getFields(), { x: c[0], y: c[1] }) as Field;
        if (fieldOnBoard.isOccupied()) {
          if (fieldOnBoard.getPlayer().getName() === player.getName()) {
            missingWinningCombinations -= 1;
          } else if (fieldOnBoard.getPlayer().getName() === opponent.getName()) {
            missingWinningCombinations = wc.length + 1;
            return false;
          }
        }
      });
      missingWinningCombinationsArray.push(missingWinningCombinations);
    });
    return _.min(missingWinningCombinationsArray);
  }
}
