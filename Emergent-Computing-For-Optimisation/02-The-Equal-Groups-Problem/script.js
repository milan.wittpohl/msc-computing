// Given

/*
Problem 1
  Groups: 9
  Blocks: 9, 9, 8, 8, 8, 7, 7, 7, 6, 6, 5, 5, 5, 5, 4, 3, 3, 2, 2, 2, 2, 2, 2
Problem 2
  Groups: 9
  Blocks: 9, 9, 8, 8, 8, 7, 7, 7, 6, 5, 5, 5, 4, 4, 4, 3, 3, 3, 3, 2, 2, 2, 2
Problem 3
  Groups: 9
  Blocks: 9, 9, 9, 9, 8, 8, 8, 7, 7, 7, 6, 6, 6, 5, 5, 5, 4
*/

let numberOfGroups // = 9
let blocks // = [9, 9, 8, 8, 8, 7, 7, 7, 6, 6, 5, 5, 5, 5, 4, 3, 3, 2, 2, 2, 2, 2, 2]

// Wanted
let target
let blocksPerGroup = []
let workloadPerGroup = []

// Strategy: Aiming For Target

function calculatetotalWorkload() {
  let totalWorkload = 0;
  blocks.forEach((block) => {
    totalWorkload += block
  })
  return totalWorkload
}

function calculateTarget(totalWorkload) {
  target = Math.round(totalWorkload / numberOfGroups)
}

function removeItemFromArray(array, item) {
  array.splice(array.indexOf(item), 1)
}

function placeBlocks(target) {
  // Stores our remaining blocks and sorts them descending, as we always want to try the largest one first
  let remaingingBlocks = _.sortBy(blocks, function (num) {
    return -num
  })
  // Solve each group
  for (var i = 1; i <= numberOfGroups; i++) {
    // Array with blocks for this group
    let blocksForGroup = []
    // Our first block is always the block with the biggest workload
    const blockWithHighestWorkload = _.max(remaingingBlocks);
    removeItemFromArray(remaingingBlocks, blockWithHighestWorkload)
    let currentWorkLoad = blockWithHighestWorkload
    blocksForGroup.push(blockWithHighestWorkload)
    // Calculate current difference to target
    let diff = Math.abs(currentWorkLoad - target)
    let noUsefulBlocksLeft = false
    // We will first try to find one block that brings us the closest to the target
    // If we find one we will look if there is another one that we can put on top
    while (!noUsefulBlocksLeft) {
      // Find single most efficient block
      let singleMostEfficientBlock = undefined
      remaingingBlocks.forEach((block) => {
        const newDiff = Math.abs((currentWorkLoad + block) - target)
        if (newDiff < diff) {
          singleMostEfficientBlock = block
          diff = newDiff
        }
      })
      // Did we find a block that makes sense?
      // If yes, add block
      // If no, stop looking
      if (singleMostEfficientBlock !== undefined) {
        removeItemFromArray(remaingingBlocks, singleMostEfficientBlock)
        currentWorkLoad += singleMostEfficientBlock
        blocksForGroup.push(singleMostEfficientBlock)
      } else {
        noUsefulBlocksLeft = true
      }
    }
    workloadPerGroup.push(currentWorkLoad)
    blocksPerGroup.push(blocksForGroup)
  }
}

function calculateDistance(workloadPGroup = workloadPerGroup) {
  return _.max(workloadPGroup) - _.min(workloadPGroup)
}

function evaluateMovingBlocksBetweenGroups(sourceGroup, targetGroup, workloadTargetGroup) {
  let errorOfTargetGroup = Math.abs(workloadTargetGroup - target)
  if (errorOfTargetGroup === 0) return
  let errorRatio = Math.round(errorOfTargetGroup / target)
  let blocksFromSourceGroupInRange = []
  sourceGroup.forEach(block => {
    let lowerBound = errorOfTargetGroup + (errorOfTargetGroup * errorRatio)
    let upperBound = errorOfTargetGroup - (errorOfTargetGroup * errorRatio)
    if ((block <= lowerBound) && (block >= upperBound)) {
      blocksFromSourceGroupInRange.push(block)
    }
  })
  blocksFromSourceGroupInRange.forEach(blockToMove => {
    console.log("Moving: ", sourceGroup, blockToMove)
    removeItemFromArray(sourceGroup, blockToMove)
    targetGroup.push(blockToMove)
  })
}

function moveBlocks() {
  let distance = calculateDistance()
  if (distance === 0) return
  blocksPerGroup.forEach(blocks => {
    blocksPerGroup.forEach(blocks2 => {
      evaluateMovingBlocksBetweenGroups(blocks, blocks2, workloadPerGroup[blocksPerGroup.indexOf(blocks2)])
    })
  })
}

function evaluateSwap(groupAIndex, groupABlock, groupBIndex, groupBBlockArray) {
  const currentDistance = calculateDistance()
  let groupAWorkload = workloadPerGroup[groupAIndex]
  groupAWorkload = groupAWorkload - groupABlock
  groupAWorkload = groupAWorkload + _.sum(groupBBlockArray)
  let groupBWorkload = workloadPerGroup[groupBIndex]
  groupBWorkload = groupBWorkload - _.sum(groupBBlockArray)
  groupBWorkload = groupBWorkload + groupABlock
  let newWorkloadPerGroup = [groupAWorkload, groupBWorkload]
  workloadPerGroup.forEach((workload, index) => {
    if (index !== groupAIndex && index !== groupBIndex) {
      newWorkloadPerGroup.push(workload)
    }
  })
  const newDistance = calculateDistance(newWorkloadPerGroup)
  return newDistance < currentDistance
}

function recalculateWorkloadPerGroup() {
  workloadPerGroup = []
  blocksPerGroup.forEach(group => {
    workloadPerGroup.push(_.sum(group))
  })
}

function swap(groupA, groupABlock, groupB, groupBBlockArray) {
  removeItemFromArray(groupA, groupABlock)
  groupBBlockArray.forEach(block => {
    removeItemFromArray(groupB, block)
  })
  groupBBlockArray.forEach(block => {
    groupA.push(block)
  })
  groupB.push(groupABlock)
  recalculateWorkloadPerGroup()
}

function evaluateSwappingOfBlocksByBruteForce() {
  let distance = calculateDistance()
  if (distance === 0) return
  let distanceImproved
  while (distanceImproved === undefined || distanceImproved === true) {
    let numberOfChecks = 0
    distanceImproved = false
    blocksPerGroup.forEach((inperfectGroup, inperfectGroupIndex) => {
      // If the group is already perfect return
      if (workloadPerGroup[inperfectGroupIndex] === target) return
      inperfectGroup.forEach(inperfectBlock => {
        blocksPerGroup.forEach((otherGroup, otherGroupIndex) => {
          // If the group is the same return
          if (inperfectGroupIndex === otherGroupIndex) return
          for (let otherBlockIndex = 0; otherBlockIndex < otherGroup.length; otherBlockIndex++) {
            for (let i = otherBlockIndex; i >= 0; i--) {
              let subsetOfOtherBlocks = _.slice(otherGroup, i, otherBlockIndex + 1)
              const shouldSwap = evaluateSwap(inperfectGroupIndex, inperfectBlock, otherGroupIndex, subsetOfOtherBlocks)
              numberOfChecks++
              if (shouldSwap) {
                console.log("Swapping!", inperfectGroup, inperfectBlock, otherGroup, subsetOfOtherBlocks)
                console.log("Previous Distance: " + calculateDistance())
                swap(inperfectGroup, inperfectBlock, otherGroup, subsetOfOtherBlocks)
                distanceImproved = true
                console.log("New Distance: " + calculateDistance())
              }
            }
          }
        })
      })
    })
    console.warn(numberOfChecks)
  }
}

// Solve

function solveByAimingForTarget() {
  const totalWorkload = calculatetotalWorkload()
  console.log("Combined Workload: " + totalWorkload)
  calculateTarget(totalWorkload)
  console.log("Target: " + target)
  placeBlocks(target)
  //moveBlocks()
  evaluateSwappingOfBlocksByBruteForce()
  console.log(blocksPerGroup)
}

function solveUsingStrategy(strategy) {
  switch (strategy) {
    case "AIMING_FOR_TARGET":
      solveByAimingForTarget()
  }
}

function solve() {
  solveUsingStrategy("AIMING_FOR_TARGET")
}

// Build canvas
function buildCanvas() {
  let htmlOfBlocks = ""
  for (var g = 0; g < numberOfGroups; g++) {
    htmlOfBlocks += "<div class='block-group'>"
    blocksPerGroup[g].forEach(block => {
      for (var i = 1; i <= block; i++) {
        htmlOfBlocks += "<div class='block length-" + block + "'></div>"
      }
    })
    htmlOfBlocks += "</div>"
  }
  document.getElementById("results").innerHTML = htmlOfBlocks
}

function showStats() {
  let totalWorkload = calculatetotalWorkload()
  document.getElementById("total-workload").innerHTML = totalWorkload
  document.getElementById("target").innerHTML = target
  let tasksPerGroupHtml = "<ul>"
  blocksPerGroup.forEach((blocks, index) => {
    let blockHtml = ""
    blocks.forEach(block => {
      blockHtml += "<li>" + block + "</li>"
    })
    tasksPerGroupHtml += "<li class='block-li'><strong>Group " + (parseInt(index) + 1) + ":</strong><ul>" + blockHtml + "</ul></li>"
  })
  tasksPerGroupHtml += "</ul>"
  document.getElementById("tasks-per-group").innerHTML = tasksPerGroupHtml
}

function buildBlockArray() {
  const input = document.getElementById('blocks').value
  let blockArrayStr = input.split(",")
  let blockArray = []
  blockArrayStr.forEach(blockStr => {
    blockArray.push(parseInt(blockStr))
  })
  return blockArray
}

document.getElementById("solve").addEventListener("click", function () {
  // Get values
  target = 0
  blocksPerGroup = []
  workloadPerGroup = []
  numberOfGroups = document.getElementById('numberOfGroups').value
  blocks = buildBlockArray()
  // Solve
  solve()
  // Build canvas
  buildCanvas()
  showStats()
});