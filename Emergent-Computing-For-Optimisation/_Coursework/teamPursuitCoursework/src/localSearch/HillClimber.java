package localSearch;

import ea.Individual;
import teamPursuit.TeamPursuit;

public class HillClimber {

    public TeamPursuit teamPursuit;

    public HillClimber(TeamPursuit teamPursuit){
        this.teamPursuit = teamPursuit;
    }

    public Individual run(int iterations, Individual individual){
        Individual optimized = individual.copy();
        boolean mutateTransition = true;
        boolean mutatePacing = true;
        for(int i = 0; i < iterations; i++) {
            Individual currentRun = optimized.copy();
            if(mutateTransition) {
                currentRun.mutateTransitionByFlippingRandom(false, 1, 6);
            }
            if(mutatePacing) {
                currentRun.mutatePacingByApplyingOffspring(false, 200, 500, 1, 6);
            }
            currentRun.evaluate(this.teamPursuit, false);
            if(currentRun.getFitness() < optimized.getFitness()) {
                optimized = currentRun;
                i = iterations;
            }
        }
        return optimized;
    }
}
