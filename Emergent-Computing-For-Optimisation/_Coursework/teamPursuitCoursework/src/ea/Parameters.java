package ea;

import java.util.Random;

public class Parameters {

	public static Random rnd = new Random(System.currentTimeMillis());

	static final boolean [] DEFAULT_WOMENS_TRANSITION_STRATEGY = {true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true};
	public static final int [] DEFAULT_WOMENS_PACING_STRATEGY = {300, 300, 300, 300, 300, 300, 300, 350, 350, 300, 300, 350, 350, 350, 350, 300, 300, 350, 350, 350, 350, 300, 300};

	public static final boolean[] SLIGHTLY_OPTIMISED_TRANSITION_STRATEGY = {true,true,false,true,true,false,false,true,false,true,false,true,false,true,true,true,true,true,true,false,true,false};

	//public static final boolean[] OPTIMISED_TRANSITION_STRATEGY = {true,false,false,true,false,false,true,false,false,true,false,false,true,false,false,true,false,false,true,false,false,true};
	//public static final int [] OPTIMISED_PACING_STRATEGY = {550, 400, 400, 400, 400, 400, 300, 400, 400, 300, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400};

	// 210 -> DEFAULT and search area HIGH_PACING_FEW_TRANSITIONS
	// 518,521,492,479,348,459,460,432,353,407,485,355,516,315,385,425,474,401,476,551,438,369,356,
	// false,false,false,true,false,false,true,false,false,false,true,false,true,false,true,true,false,false,true,false,true,false,
	//public static final boolean[] OPTIMISED_TRANSITION_STRATEGY = {false,true,false,false,false,false,false,false,false,false,true,false,false,false,false,false,false,false,true,false,true,false};
	public static final boolean[] OPTIMISED_TRANSITION_STRATEGY = {true,true,false,false,false,false,false,true,false,false,false,false,false,false,true,false,false,false,false,false,false,false};
	//public static final int [] OPTIMISED_PACING_STRATEGY = {518,521,492,479,348,459,460,432,353,407,485,355,516,315,385,425,474,401,476,551,438,369,356};
	//public static final int [] OPTIMISED_PACING_STRATEGY = {700, 600, 600, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 300, 300, 300, 300, 300, 300, 300, 300};
	public static final int [] OPTIMISED_PACING_STRATEGY = {971,341,345,364,545,447,426,441,471,320,433,302,432,439,490,395,544,435,362,399,366,412,292};

	public static enum SearchArea{
		//NONE(0, 200, 600, 0, 0),
		NONE(0, 200, 550, 0, 0),
		DEFAULT(400, 200, 600, 10, 8),
		LOW_PACING(300, 200, 400, 10, 8),
		HIGH_PACING(300, 200, 500, 0, 0),
		FEW_TRANSITIONS(350, 300, 550, 4,4),
		MORE_TRANSITIONS(400, 300, 500, 15, 12),
		LOW_PACING_FEW_TRANSITIONS(300, 200, 400, 5, 4),
		//HIGH_PACING_FEW_TRANSITIONS(300, 200, 550, 5, 3),
		//HIGH_PACING_FEW_TRANSITIONS(400, 300, 550, 5, 4),
		HIGH_PACING_FEW_TRANSITIONS(450, 200, 900, 3, 2),
		LOW_PACING_MORE_TRANSITIONS(300, 200, 400, 15, 12),
		HIGH_PACING_MORE_TRANSITIONS(300, 300, 550, 20, 12);


		public final int pacingAvg;
		public final int pacingMaxValue;
		public final int pacingMinValue;
		public final int transitions;
		public final int transitionDistribution;

		public final int pacingAvgLower;
		public final int pacingAvgUpper;
		public final int transitionTransitionsLower;
		public final int transitionTransitionsUpper;
		public final int transitionTransitionDistributionLower;
		public final int transitionTransitionDistributionUpper;

		SearchArea(int pacingAvg, int pacingMinValue, int pacingMaxValue, int transitions, int transitionDistribution) {
			this.pacingAvg = pacingAvg;
			this.pacingMinValue = pacingMinValue;
			this.pacingMaxValue = pacingMaxValue;
			this.transitions = transitions;
			this.transitionDistribution = transitionDistribution;

			this.pacingAvgLower = Math.toIntExact(Math.round(pacingAvg * 0.75));
			this.pacingAvgUpper = Math.toIntExact(Math.round(pacingAvg * 1.0));
			this.transitionTransitionsLower = Math.toIntExact(Math.round(Double.valueOf(transitions) * 1.0));
			this.transitionTransitionsUpper = this.transitions == 0 ? 22 : Math.toIntExact(Math.round(Double.valueOf(transitions) * 1.5));
			this.transitionTransitionDistributionLower = Math.toIntExact(Math.round(Double.valueOf(transitionDistribution) * 0.5));
			this.transitionTransitionDistributionUpper = this.transitions == 0 ? 22 : Math.toIntExact(Math.round(Double.valueOf(transitionDistribution) * 1.5));

		}

	}

    public static enum Strategy {
		//DEFAULT(0, 1000, 50, 1, 6, 1.0, 0.5, 1000, 100,true, true, true, true, true, true, true), //LONG
		DEFAULT(0, 100, 5, 1, 2, 1.0, 0.5, 100, 10,true, true, true, true, true, true, true), // SHORT
		//NONE(0, 1000, 10, 4, 8, 1.0, 1.0, 20, 20,true, true, true, true, true, true, true),
        PACING_AVG(10, 1000, 0, 1, 6, 1.0, 0.8, 100, 10,true, true, true, true, true, true, true),
        PACING_MIN_MAX(10, 1000, 0, 1, 6, 1.0, 0.8, 100, 10,true, true, true, true, true, true, true),
        TRANSITION_TRUES(2, 100, 0, 1, 3, 1.0, 0.5, 100, 1000,true, true, true, true, true, true, true),
		TRANSITION_DISTRIBUTION(1, 1000, 0, 1, 6, 1.0, 0.8, 100, 10,true, true, true, true, true, true, true);
		//TRANSITION_DISTRIBUTION(2, 100, 0, 1, 6, 1.0, 0.5, 100, 10,true, true, true, true, true, true, true);

        public final int radius;
        public final int populationSize;
        public final int tournamentSize;
        public final int minMutation;
        public final int maxMutations;
		public final double crossoverProbability;
        public final double mutationProbability;
        public final int iterations;
        public final int numberOfChildren;
        public final boolean runHillClimber;
		public final boolean randomisePacing;
		public final boolean randomiseTransition;
		public final boolean crossoverPacing;
		public final boolean crossoverTransition;
		public final boolean mutatePacing;
		public final boolean mutateTransition;

		private Strategy(int radius, int populationSize, int tournamentSize, int minMutations, int maxMutations, double crossoverProbability, double mutationProbability, int iterations, int numberOfChildren, boolean runHillClimber, boolean randomisePacing, boolean randomiseTransition, boolean crossoverPacing, boolean crossoverTransition, boolean mutatePacing, boolean mutateTransition) {
			this.radius = radius;
			this.populationSize = populationSize;
			this.tournamentSize = tournamentSize;
			this.minMutation = minMutations;
			this.maxMutations = maxMutations;
			this.crossoverProbability = crossoverProbability;
			this.mutationProbability = mutationProbability;
			this.iterations = iterations;
			this.numberOfChildren = numberOfChildren;
			this.runHillClimber = runHillClimber;
			this.randomisePacing = randomisePacing;
			this.randomiseTransition = randomiseTransition;
			this.crossoverPacing = crossoverPacing;
			this.crossoverTransition = crossoverTransition;
			this.mutatePacing = mutatePacing;
			this.mutateTransition = mutateTransition;
		}
    }
	
	
}
