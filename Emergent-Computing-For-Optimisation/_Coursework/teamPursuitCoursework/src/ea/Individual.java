package ea;

import com.sun.deploy.util.ArrayUtil;
import com.sun.tools.javac.util.ArrayUtils;
import lab.Lab;
import localSearch.HillClimber;
import teamPursuit.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Individual {

	int transitionStrategySpecies = Integer.MIN_VALUE;
	public boolean[] transitionStrategy = new boolean[22] ;
	int pacingStrategySpecies = Integer.MIN_VALUE;
	public int[] pacingStrategy = new int[23];
	int iterationBorn;
	TeamPursuit teamPursuit;
	private Parameters.Strategy sharedFitness;
	private Parameters.SearchArea searchArea;
	SimulationResult result = null;
	Double sharedFitnessValue = null;
	
	public Individual(int iterationBorn, TeamPursuit teamPursuit, Parameters.Strategy sharedFitness, Parameters.SearchArea searchArea) {
		this.iterationBorn = iterationBorn;
		this.teamPursuit = teamPursuit;
		this.sharedFitness = sharedFitness;
		this.searchArea = searchArea;
	}

	// this code just evolves the transition strategy
	// an individual is initialised with a random strategy that will evolve
	// the pacing strategy is initialised to the default strategy and remains fixed
	
	public void initialise(boolean randomiseTransition, boolean buildTransitionInSegments, boolean randomisePacing, boolean buildPacingInSegments){

		if(!randomiseTransition){
			for(int i = 0; i < transitionStrategy.length; i++){
				transitionStrategy[i] = Parameters.OPTIMISED_TRANSITION_STRATEGY[i];
			}
		}
		if(!randomisePacing){
			for(int i = 0; i < pacingStrategy.length; i++){
				pacingStrategy[i] = Parameters.OPTIMISED_PACING_STRATEGY[i];
			}
		}

		if(randomiseTransition && buildTransitionInSegments){
			boolean[] seg1 = buildTransitionStrategySegment(2, 2, 2);
			boolean[] seg2 = buildTransitionStrategySegment(20, 1, 3);
			//boolean[] seg3 = buildTransitionStrategySegment(10, 1, 3);
			addTransitionSegment(seg1, transitionStrategy, 0);
			addTransitionSegment(seg2, transitionStrategy, seg1.length);
			//addTransitionSegment(seg3, transitionStrategy, seg1.length + seg2.length);
		}else if(randomiseTransition && !buildTransitionInSegments){
			boolean[] seg1 = buildTransitionStrategySegment(22, searchArea.transitionTransitionsLower, searchArea.transitionTransitionsUpper);
			addTransitionSegment(seg1, transitionStrategy, 0);
			attemptToFixTransitionAccordingToSearchArea();
		}

		if(randomisePacing && buildPacingInSegments){
			int[] seg1 = buildPacingStrategySegment(1, 600, 1000);
			int[] seg2 = buildPacingStrategySegment(22, 275, 500);
			//int[] seg3 = buildPacingStrategySegment(12, 200, 350);
			addPacingSegment(seg1, pacingStrategy, 0);
			addPacingSegment(seg2, pacingStrategy, seg1.length);
			//addPacingSegment(seg3, pacingStrategy, seg1.length + seg2.length);
		} else if(randomisePacing && !buildPacingInSegments){
			for(int i = 0; i < pacingStrategy.length; i++){
				pacingStrategy[i] = Parameters.rnd.ints(1, searchArea.pacingMinValue, searchArea.pacingMaxValue + 1).toArray()[0];
			}
			attemptToFixPacingAccordingToSearchArea();
		}


		setSpecies();

	}

	private boolean[] buildTransitionStrategySegment(int length, int lowerBound, int upperBoud){
		Boolean[] segment = new Boolean[length];
		int transitions = 0;
		for(int i = 0; i < length; i++){
			if(searchArea == Parameters.SearchArea.NONE){
				boolean candidate = Parameters.rnd.nextBoolean();
				segment[i] = candidate;
			}else{
				if(transitions <= lowerBound){
					segment[i] = true;
					transitions++;
				}
				if(transitions > lowerBound){
					segment[i] = false;
				}
			}
		}
		List<Boolean> valueList = Arrays.asList(segment);
		Collections.shuffle(valueList);
		return Lab.toPrimitiveArray(valueList);
	}

	private int[] buildPacingStrategySegment(int length, int lowerBound, int upperBoud){
		int[] segment = new int[length];
		for(int i = 0; i < length; i++){
			segment[i] = Parameters.rnd.ints(1, lowerBound, upperBoud + 1).toArray()[0];
		}
		return segment;
	}

	private void addTransitionSegment(boolean[] segement, boolean[] target, int startIndex){
		for(int i = startIndex; i < segement.length + startIndex; i++){
			target[i] = segement[i - startIndex];
		}
	}

	private void addPacingSegment(int[] segement, int[] target, int startIndex){
		for(int i = startIndex; i < segement.length + startIndex; i++){
			target[i] = segement[i - startIndex];
		}
	}

	// FIXME: SPECIES

	public void setSpecies(){
		switch (sharedFitness){
			case PACING_AVG:
				pacingStrategySpecies = getPacingSpeciesBasedOnAvgPacingDifference();
				break;
			case PACING_MIN_MAX:
				pacingStrategySpecies = getPacingSpeciesBasedOnPacingDifference();
				break;
			case TRANSITION_TRUES:
				transitionStrategySpecies = getTransitionSpeciesBasedOnNumberOfTrues();
				break;
			case TRANSITION_DISTRIBUTION:
				transitionStrategySpecies = getTransitionSpeciesBasedOnDistribution();
				break;
		}
	}

	public int getTransitionSpeciesBasedOnDistribution(){
		int max = 0;
		int count = 0;
		for(int i = 1; i < transitionStrategy.length; i++){
			if(transitionStrategy[i - 1] == transitionStrategy[i]){
				count++;
			}else{
				max = Math.max(max, count);
				count = 0;
			}
		}
		return max;
	}

	public int getNumberOfTruesOfTransitionStrategy(){
		int numberOfTrues = 0;
		for(int i = 0; i < transitionStrategy.length; i++){
			if(transitionStrategy[i]) numberOfTrues++;
		}
		return numberOfTrues;
	}

	private int getTransitionSpeciesBasedOnNumberOfTrues(){
		return getNumberOfTruesOfTransitionStrategy();
	}

	public int getPacingSpeciesBasedOnPacingDifference(){
		int[] maxAndMinDiff = calculateMaxAndMinPacingDifference();
		int diff = maxAndMinDiff[0] - maxAndMinDiff[1];
		return diff;
	}

	public int getPacingSpeciesBasedOnAvgPacingDifference(){
		return calculateAvgPacingDifference();
	}

	// FIXME: Search Area

	public void attemptToFixTransitionAccordingToSearchArea(){
		if(searchArea == Parameters.SearchArea.NONE){
			return;
		}
		int count = 0;
		int transitions = getNumberOfTruesOfTransitionStrategy();
		int missingTransitions = searchArea.transitionTransitionsLower - transitions;
		int tooManyTransitions = transitions - searchArea.transitionTransitionsUpper;
		int newTransitions = 0;
		while(count <= 100 && (transitions < searchArea.transitionTransitionsLower || transitions > searchArea.transitionTransitionsUpper)){
			if(transitions < searchArea.transitionTransitionsLower){
				for(int i = 0; i < transitionStrategy.length; i++){
					if(!transitionStrategy[i]) {
						transitionStrategy[i] = true;
						newTransitions++;
					}
					if(newTransitions == missingTransitions) i = transitionStrategy.length;
				}
			}else{
				for(int i = 0; i < transitionStrategy.length; i++){
					if(transitionStrategy[i]) {
						transitionStrategy[i] = false;
						newTransitions++;
					}
					if(newTransitions == tooManyTransitions) i = transitionStrategy.length;
				}
			}
			transitions = getNumberOfTruesOfTransitionStrategy();
			count++;
			if(count == 100 && (transitions < searchArea.transitionTransitionsLower || transitions > searchArea.transitionTransitionsUpper)){
				System.out.println("WARNING: Could not modify transition strategy to conform to desired number of transitions!");
			}
		}

		int distribution = getTransitionSpeciesBasedOnDistribution();
		count = 0;
		while(count <= 1000 && (distribution < searchArea.transitionTransitionDistributionLower || distribution > searchArea.transitionTransitionDistributionUpper)){
			transitionStrategy = RandomizeBooleanArray(transitionStrategy);
			distribution = getTransitionSpeciesBasedOnDistribution();
			count++;
			if(count == 100 && (distribution < searchArea.transitionTransitionDistributionLower || distribution > searchArea.transitionTransitionDistributionUpper)){
				System.out.println("WARNING: Could not modify transition strategy to conform to desired distribution!");
			}
		}
	}

	public void attemptToFixPacingAccordingToSearchArea(){
		if(searchArea == Parameters.SearchArea.NONE){
			return;
		}
		int pacingAvg = calculateAvgPacingDifference();
		int count = 0;
		while(count <= 1000 && (pacingAvg < searchArea.pacingAvgLower || pacingAvg > searchArea.pacingAvgUpper)){
			if(pacingAvg < searchArea.pacingAvgLower){
				int index = Parameters.rnd.nextInt(pacingStrategy.length);
				int offset = pacingStrategy[index] + 50 > 1200 ? 1200 - pacingStrategy[index] : 50;
				pacingStrategy[index] = pacingStrategy[index] + offset;
			}else{
				int index = Parameters.rnd.nextInt(pacingStrategy.length);
				int offset = pacingStrategy[index] - 50 < 200 ? pacingStrategy[index] - 200 : 30;
				pacingStrategy[index] = pacingStrategy[index] - offset;
			}
			pacingAvg = calculateAvgPacingDifference();
			count++;
			if(count == 1000 && (pacingAvg < searchArea.pacingAvgLower || pacingAvg > searchArea.pacingAvgUpper)){
				System.out.println("WARNING: Could not modify pacing strategy to conform to desired average value!");
			}
		}
	}

	// FIXME: MUTATION

	public void mutateTransitionByFlippingRandom(boolean evenOut, int mutationRateMin, int mutationRateMax) {
		int mutationTransitionRate = mutationRateMin + Parameters.rnd.nextInt(mutationRateMax);
		mutateTransitionByFlippingRandom(mutationTransitionRate, evenOut);
		attemptToFixTransitionAccordingToSearchArea();
	}

	private void mutateTransitionByFlippingRandom(int mutationTransitionRate, boolean evenOut) {
		//mutate the transition strategy by flipping boolean value
		for(int n = 0; n < mutationTransitionRate; n++){
			int index = Parameters.rnd.nextInt(this.transitionStrategy.length);
			if(evenOut){
				int index2 = index;
				int index3 = -1;
				int offset = 1;
				while(index3 == -1){
					if(index2 + offset > transitionStrategy.length - 1){
						offset = 0;
						index2 = 0;
					}
					int candidate = index2 + offset;
					if(candidate != index){
						boolean candidateValue = this.transitionStrategy[candidate];
						if(candidateValue != this.transitionStrategy[index]){
							index3 = candidate;
						}
					}
					offset++;
				}
				this.transitionStrategy[index3] = !this.transitionStrategy[index3];
			}
			this.transitionStrategy[index] = !this.transitionStrategy[index];
		}
	}

	public void mutatePacingBySwappingRandom(int mutationRateMin, int mutationRateMax) {
		int mutationPacingRate = mutationRateMin + Parameters.rnd.nextInt(mutationRateMax);
		mutatePacingBySwappingRandoms(mutationPacingRate);
		attemptToFixPacingAccordingToSearchArea();
	}

	private void mutatePacingBySwappingRandoms(int mutationPacingRate) {
		for(int n = 0; n < mutationPacingRate; n++){
			int index1 = Parameters.rnd.nextInt(this.pacingStrategy.length);
			int index2 = Parameters.rnd.nextInt(this.pacingStrategy.length);
			int temp = this.pacingStrategy[index1];
			this.pacingStrategy[index1] = this.pacingStrategy[index2];
			this.pacingStrategy[index2] = temp;
		}
	}

	public void mutatePacingByApplyingOffspring(boolean evenOut, int lowerBound, int upperBound, int mutationRateMin, int mutationRateMax) {
		int mutationPacingRate = mutationRateMin + Parameters.rnd.nextInt(mutationRateMax);
		mutatePacingByApplyingOffspring(mutationPacingRate, evenOut, lowerBound, upperBound);
		attemptToFixPacingAccordingToSearchArea();
	}

	private void mutatePacingByApplyingOffspring(int mutationPacingRate, boolean evenOut, int lowerBound, int upperBound) {
		for(int n = 0; n < mutationPacingRate; n++){
			int index = Parameters.rnd.nextInt(this.pacingStrategy.length);
			int negate = Parameters.rnd.nextDouble() > 0.5 ? -1 : 1;
			Double offspring = 0.05 * negate;
			int newPacing = (int) Math.round(this.pacingStrategy[index] + (offspring * this.pacingStrategy[index]));
			if(newPacing >= 200 && newPacing <= 1200 && newPacing >= lowerBound && newPacing <= upperBound && this.pacingStrategy[index] != lowerBound && this.pacingStrategy[index] != upperBound){
				if(evenOut){
					int diff = newPacing - this.pacingStrategy[index];
					int index2 = index;
					int index3 = -1;
					int offset = 1;
					while(index3 == -1){
						if(index2 + offset > transitionStrategy.length - 1){
							offset = 0;
							index2 = 0;
						}
						int candidate = index2 + offset;
						if(candidate != index){
							int candidateValue = this.pacingStrategy[candidate] - diff;
							if(candidateValue >= 200 && candidateValue <= 1200){
								index3 = candidate;
							}
						}
						offset++;
					}
					this.pacingStrategy[index3] = this.pacingStrategy[index3] - diff;
				}
				this.pacingStrategy[index] = newPacing;
			}
		}
	}

	// FIXME: EVALUATION

	public void evaluate(TeamPursuit teamPursuit){
		evaluate(teamPursuit, false);
	}

	public int evaluate(TeamPursuit teamPursuit, boolean runHillClimber){
		int hcSucceeded = -1;
		try {
			result = teamPursuit.simulate(transitionStrategy, pacingStrategy);
			if(result.getProportionCompleted() >= 0.93 && result.getFinishTime() == Double.POSITIVE_INFINITY && runHillClimber){
				HillClimber hillClimber = new HillClimber(teamPursuit);
				Individual bestPostHC = hillClimber.run(10, this);
				result = bestPostHC.result;
				transitionStrategy = bestPostHC.transitionStrategy;
				pacingStrategy = bestPostHC.pacingStrategy;
				if(result.getFinishTime() == Double.POSITIVE_INFINITY){
					//System.out.println("Ran HC UNSUCCESSFULLY");
					hcSucceeded = 0;
				}else{
					//System.out.println("Ran HC and improved result.");
					hcSucceeded = 1;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hcSucceeded;
	}

	public void evaluateSharedFitness(TeamPursuit teamPursuit, ArrayList<Individual> individualsInRange, Parameters.Strategy strategy){
		int r = sharedFitness.radius == 0 ? 1 : strategy.radius;
		switch (strategy){
			case PACING_MIN_MAX:
			case PACING_AVG:
				if(this.result == null){
					this.evaluate(teamPursuit);
				}
				int ratio = 0;
				for (Individual individual : individualsInRange) {
					int distance = Math.abs(this.pacingStrategySpecies - individual.pacingStrategySpecies);
					ratio = ratio + 1 - (distance / r);
				}
				Double multiplier = 1 + (ratio / this.result.getFinishTime());
				this.sharedFitnessValue = this.result.getFinishTime() * multiplier;
				break;
			case TRANSITION_TRUES:
			case TRANSITION_DISTRIBUTION:
				if(this.result == null){
					this.evaluate(teamPursuit);
				}
				ratio = 0;
				for (Individual individual : individualsInRange) {
					int distance = Math.abs(this.transitionStrategySpecies - individual.transitionStrategySpecies);
					ratio = ratio + 1 - (distance / r);
				}
				multiplier = 1 + (ratio / this.result.getFinishTime());
				this.sharedFitnessValue = this.result.getFinishTime() * multiplier;
				break;
		}
	}

	// FIXME: HELPERS

	private int calculateAvgPacingDifference() {
		int avgDifference = Arrays.stream(this.pacingStrategy).sum() / this.pacingStrategy.length;
		return avgDifference;
	}

	public int[] calculateMaxAndMinPacingDifference() {
		int max = Arrays.stream(this.pacingStrategy).max().getAsInt();
		int min = Arrays.stream(this.pacingStrategy).min().getAsInt();
		return new int[]{max, min};
	}


	// this is a very basic fitness function
	// if the race is not completed, the chromosome gets fitness 1000
	// otherwise, the fitness is equal to the time taken
	// chromosomes that don't complete all get the same fitness (i.e regardless of whether they 
	// complete 10% or 90% of the race

	public double getFitness(){
		return getFitness(true);
	}
	
	public double getFitness(boolean sharedFitness){
		if(sharedFitnessValue != null && sharedFitness){
			return sharedFitnessValue;
		}
		double fitness = 1000;
		if (result == null || result.getProportionCompleted() < 0.999){
			return fitness;
		}
		else{				
			fitness = result.getFinishTime();
		}
		return fitness;
	}
	
	
	
	public Individual copy(){
		Individual individual = new Individual(this.iterationBorn, teamPursuit, sharedFitness, searchArea);
		for(int i = 0; i < transitionStrategy.length; i++){
			individual.transitionStrategy[i] = transitionStrategy[i];
		}		
		for(int i = 0; i < pacingStrategy.length; i++){
			individual.pacingStrategy[i] = pacingStrategy[i];
		}		
		individual.evaluate(EA.teamPursuit, false);
		return individual;
	}
	
	@Override
	public String toString() {
		String str = "";
		if(result != null){
			str += getFitness();
		}
		return str;
	}

	public void print() {
		for(int i : pacingStrategy){
			System.out.print(i + ",");			
		}
		System.out.println();
		for(boolean b : transitionStrategy){
			if(b){
				System.out.print("true,");
			}else{
				System.out.print("false,");
			}
		}
		System.out.println("\r\n" + this);
	}

	public static int[] RandomizeIntArray(int[] array){
		Random rgen = new Random();  // Random number generator

		for (int i=0; i<array.length; i++) {
			int randomPosition = rgen.nextInt(array.length);
			int temp = array[i];
			array[i] = array[randomPosition];
			array[randomPosition] = temp;
		}

		return array;
	}

	public static boolean[] RandomizeBooleanArray(boolean[] array){
		Random rgen = new Random();  // Random number generator

		for (int i=0; i<array.length; i++) {
			int randomPosition = rgen.nextInt(array.length);
			boolean temp = array[i];
			array[i] = array[randomPosition];
			array[randomPosition] = temp;
		}

		return array;
	}

	// this is just there in case you want to check the default strategies
	public void initialise_default(){
		for(int i = 0; i < transitionStrategy.length; i++){
			transitionStrategy[i] = Parameters.DEFAULT_WOMENS_TRANSITION_STRATEGY[i];
		}

		for(int i = 0; i < pacingStrategy.length; i++){
			pacingStrategy[i] = Parameters.DEFAULT_WOMENS_PACING_STRATEGY[i];
		}

	}
}
