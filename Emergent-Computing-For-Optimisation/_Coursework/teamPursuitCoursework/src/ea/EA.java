package ea;

/***
 * This is an example of an EA used to solve the problem
 *  A chromosome consists of two arrays - the pacing strategy and the transition strategy
 * This algorithm is only provided as an example of how to use the code and is very simple - it ONLY evolves the transition strategy and simply sticks with the default
 * pacing strategy
 * The default settings in the parameters file make the EA work like a hillclimber:
 * 	the population size is set to 1, and there is no crossover, just mutation
 * The pacing strategy array is never altered in this version- mutation and crossover are only
 * applied to the transition strategy array
 * It uses a simple (and not very helpful) fitness function - if a strategy results in an
 * incomplete race, the fitness is set to 1000, regardless of how much of the race is completed
 * If the race is completed, the fitness is equal to the time taken
 * The idea is to minimise the fitness value
 */


import java.util.*;
import java.util.stream.Collectors;

import lab.Lab;
import localSearch.HillClimber;
import teamPursuit.SimulationResult;
import teamPursuit.TeamPursuit;
import teamPursuit.WomensTeamPursuit;

public class EA{
	
	// create a new team with the default settings
	protected static TeamPursuit teamPursuit = new WomensTeamPursuit();
    private static ArrayList<Individual> bestResults = new ArrayList<>();
    private static ArrayList<Individual> bestResultsOfEA = new ArrayList<>();
	private ArrayList<Individual> population = new ArrayList<Individual>();
	private int iteration = 0;
    private Parameters.Strategy strategy;
    private Parameters.SearchArea searchArea;
    private boolean segements = true;

	// FIXME: *** RUN ALGORITHM ***
	
	public EA() {
		
	}

	
	public static void main(String[] args) {
	    String runs = args[0];
        String strategyStr = args[1];
        String searchSpaceStr = args[2];
        Parameters.Strategy strategy = Parameters.Strategy.DEFAULT;
        if(strategyStr.equals("PACING_AVG")){
            strategy = Parameters.Strategy.PACING_AVG;
        }else if(strategyStr.equals("PACING_MIN_MAX")){
            strategy = Parameters.Strategy.PACING_MIN_MAX;
        }else if(strategyStr.equals("TRANSITION_TRUES")){
            strategy = Parameters.Strategy.TRANSITION_TRUES;
        }else if(strategyStr.equals("TRANSITION_DISTRIBUTION")){
            strategy = Parameters.Strategy.TRANSITION_DISTRIBUTION;
        }
        Parameters.SearchArea searchArea = Parameters.SearchArea.NONE;
        if(searchSpaceStr.equals("DEFAULT")){
            searchArea = Parameters.SearchArea.DEFAULT;
        }else if(searchSpaceStr.equals("LOW_PACING")){
            searchArea = Parameters.SearchArea.LOW_PACING;
        }else if(searchSpaceStr.equals("HIGH_PACING")){
            searchArea = Parameters.SearchArea.HIGH_PACING;
        }else if(searchSpaceStr.equals("FEW_TRANSITIONS")){
            searchArea = Parameters.SearchArea.FEW_TRANSITIONS;
        }else if(searchSpaceStr.equals("MORE_TRANSITIONS")){
            searchArea = Parameters.SearchArea.MORE_TRANSITIONS;
        }else if(searchSpaceStr.equals("LOW_PACING_FEW_TRANSITIONS")){
            searchArea = Parameters.SearchArea.LOW_PACING_FEW_TRANSITIONS;
        }else if(searchSpaceStr.equals("HIGH_PACING_FEW_TRANSITIONS")){
            searchArea = Parameters.SearchArea.HIGH_PACING_FEW_TRANSITIONS;
        }else if(searchSpaceStr.equals("LOW_PACING_MORE_TRANSITIONS")){
            searchArea = Parameters.SearchArea.LOW_PACING_MORE_TRANSITIONS;
        }else if(searchSpaceStr.equals("HIGH_PACING_MORE_TRANSITIONS")){
            searchArea = Parameters.SearchArea.HIGH_PACING_MORE_TRANSITIONS;
        }

        runEAs(Integer.parseInt(runs), strategy, searchArea);
		// exploreLandscape();
        //evaluateFixedStrategies();
	}

	private static void evaluateFixedStrategies(){
	    int[] pacing = {971,341,345,364,545,447,426,441,471,320,433,302,432,439,490,395,544,435,362,399,366,412,292};
	    boolean[] transitions = {true,true,false,false,false,false,false,true,false,false,false,false,false,false,true,false,false,false,false,false,false,false};
	    Individual individual = new Individual(1, teamPursuit, Parameters.Strategy.DEFAULT , Parameters.SearchArea.NONE);
	    individual.pacingStrategy = pacing;
	    individual.transitionStrategy = transitions;
	    individual.evaluate(teamPursuit, false);
	    System.out.println(individual.result.getFinishTime());
    }

	private static void runEAs(int runs, Parameters.Strategy strategy, Parameters.SearchArea searchArea){
		EA ea = new EA();
        bestResultsOfEA = new ArrayList<Individual>();
        ArrayList<Integer[]> stats = new ArrayList<>();
        System.out.println("Starting application for " + runs + " runs.");
        for(int i = 0; i < runs; i++){
		    System.out.println("Starting run " + i + " with strategy " + strategy.toString() + " and search area " + searchArea.toString());
			bestResults.add(ea.runEA(strategy, searchArea));
			Integer avgPacing = bestResults.get(i).getPacingSpeciesBasedOnAvgPacingDifference();
			Integer minMaxPacing = bestResults.get(i).getPacingSpeciesBasedOnPacingDifference();
			Integer numberOfTrues = bestResults.get(i).getNumberOfTruesOfTransitionStrategy();
			Integer transitionDistribution = bestResults.get(i).getTransitionSpeciesBasedOnDistribution();
			stats.add(new Integer[]{avgPacing, minMaxPacing, numberOfTrues, transitionDistribution});
            System.out.println("Done running EA #" + i + " with strategy " + strategy.toString() + " and search area " + searchArea.toString());
		}
		Individual best = getOverallBest(bestResults);
		printBestResults(bestResults);
		best.print();
	}

	public Individual runEA(Parameters.Strategy sharedFitnessFunction, Parameters.SearchArea searchArea) {
        population = new ArrayList<Individual>();
        this.strategy = sharedFitnessFunction;
        this.searchArea = searchArea;
        HillClimber hillClimber = new HillClimber(teamPursuit);
		initialisePopulation();
		System.out.println("finished init pop");
		iteration = -1;
        long t= System.currentTimeMillis();
        long end = t + 300000;
        while(System.currentTimeMillis() < end) {
		//while(iteration < strategy.iterations){
			iteration++;
            if(strategy == Parameters.Strategy.DEFAULT){
                tournamentIteration();
            }else{
                tournamentSpeciesIteration();
            }
            bestResultsOfEA.add(getBest(population));
            printFintessDistribution();
		}
        Individual best = getBest(population);
		if(strategy.runHillClimber){
            Individual bestPostHC = hillClimber.run(100, best);
            bestPostHC.print();
            return bestPostHC;
        }
		return best;

	}

    // FIXME: *** Create initial population ***

    private void initialisePopulation() {
	    int hcSuccess = 0;
	    int hcUnSuccess = 0;
        while(population.size() < strategy.populationSize){
            Individual individual = new Individual(iteration, teamPursuit, strategy, searchArea);
            individual.initialise(strategy.randomiseTransition, segements, strategy.randomisePacing, segements);
            int hcResult = individual.evaluate(teamPursuit, strategy.runHillClimber);
            if(hcResult == 1) hcSuccess++;
            if(hcResult == 0) hcUnSuccess++;
            population.add(individual);
        }
        if(strategy != Parameters.Strategy.DEFAULT){
            calculatePopulationsSharedFitness();
        }
        System.out.println("Ran HC " + (hcSuccess + hcUnSuccess) + " times - " + hcSuccess + " - " + hcUnSuccess);
    }

    private void calculatePopulationsSharedFitness() {
        population.forEach(individual -> {
            ArrayList<Individual> individualsInRange = getIndividualsInRangeForSharedFitness(individual.pacingStrategySpecies);
            individual.evaluateSharedFitness(teamPursuit, individualsInRange, strategy);
        });
    }

    // FIXME: *** Select Parents ***

    private void tournamentIteration() {
	    for(int i = 0; i < strategy.numberOfChildren; i++) {
            Individual parent1 = tournamentSelection();
            Individual parent2 = tournamentSelection();
            Individual child = crossover(parent1, parent2);
            child = mutate(child);
            //int hcResult = child.evaluate(teamPursuit, strategy.runHillClimber);
            replace(child);
            // forceReplaceByRandom(child);
            //if(hcResult > -1) System.out.println("Ran HC to improve child " + hcResult);
        }
        System.out.println("Made " + strategy.numberOfChildren + " new children.");
        replaceOldestIfStuckForNIterations(10, 1.0, Math.toIntExact(Math.round(0.05 * strategy.populationSize)));
        replaceOldestIfStuckForNIterations(15, 1.0, Math.toIntExact(Math.round(0.1 * strategy.populationSize)));
        //replaceAllButNBestsForThresholdAfterMIterations(100, 1.0, 5);
        printStats();
    }

    private void tournamentSpeciesIteration() {
        int newChildren = 0;
        switch(strategy){
            case PACING_AVG:
                for(int i = strategy.radius; i < 1200 - strategy.radius; i = i + (strategy.radius * 2)){
                    ArrayList<Individual> individualsOfSpeciesInRange = getIndividualsInRangeForSharedFitness(i);
                    if(individualsOfSpeciesInRange.size() > 0){
                        int numberOfChildren = Math.toIntExact(Math.round(0.005 * strategy.populationSize * (1 - Double.valueOf(Double.valueOf(individualsOfSpeciesInRange.size()) / Double.valueOf(population.size())))));
                        for(int n = 0; n < numberOfChildren; n++){
                            Individual parent1 = tournamentSelectionWithRegardToSpecies(individualsOfSpeciesInRange);
                            Individual parent2 = tournamentSelectionWithRegardToSpecies(individualsOfSpeciesInRange);
                            Individual child = crossover(parent1, parent2);
                            child = mutate(child);
                            child.evaluateSharedFitness(teamPursuit, individualsOfSpeciesInRange, strategy);
                            replaceInSpecies(child, individualsOfSpeciesInRange);
                            newChildren++;
                        }
                    }
                }
                break;
            case PACING_MIN_MAX:
                for(int i = strategy.radius; i < 1000 - strategy.radius; i = i + (strategy.radius * 2)){
                    ArrayList<Individual> individualsOfSpeciesInRange = getIndividualsInRangeForSharedFitness(i);
                    if(individualsOfSpeciesInRange.size() > 0){
                        int numberOfChildren = Math.toIntExact(Math.round(0.005 * strategy.populationSize * (1 - Double.valueOf(Double.valueOf(individualsOfSpeciesInRange.size()) / Double.valueOf(population.size())))));
                        for(int n = 0; n < numberOfChildren; n++){
                            Individual parent1 = tournamentSelectionWithRegardToSpecies(individualsOfSpeciesInRange);
                            Individual parent2 = tournamentSelectionWithRegardToSpecies(individualsOfSpeciesInRange);
                            Individual child = crossover(parent1, parent2);
                            child = mutate(child);
                            child.evaluateSharedFitness(teamPursuit, individualsOfSpeciesInRange, strategy);
                            replaceInSpecies(child, individualsOfSpeciesInRange);
                            newChildren++;
                        }
                    }
                }
                break;
            case TRANSITION_TRUES:
            case TRANSITION_DISTRIBUTION:
                for(int i = strategy.radius; i < 22 - strategy.radius; i = i + (strategy.radius * 2)){
                    ArrayList<Individual> individualsOfSpeciesInRange = getIndividualsInRangeForSharedFitness(i);
                    if(individualsOfSpeciesInRange.size() > 0){
                        int numberOfChildren = Math.toIntExact(Math.round(0.2 * strategy.populationSize * (1 - Double.valueOf(Double.valueOf(individualsOfSpeciesInRange.size()) / Double.valueOf(population.size())))));
                        for(int n = 0; n < numberOfChildren; n++){
                            Individual parent1 = tournamentSelectionWithRegardToSpecies(individualsOfSpeciesInRange);
                            Individual parent2 = tournamentSelectionWithRegardToSpecies(individualsOfSpeciesInRange);
                            Individual child = crossover(parent1, parent2);
                            child = mutate(child);
                            child.evaluateSharedFitness(teamPursuit, individualsOfSpeciesInRange, strategy);
                            replaceInSpecies(child, individualsOfSpeciesInRange);
                            newChildren++;
                        }
                    }
                }
                break;
        }
        System.out.println("Made " + newChildren + " new children.");
        replaceOldestIfStuckForNIterations(5, 1.0, Math.toIntExact(Math.round(0.05 * strategy.populationSize)));
        replaceOldestIfStuckForNIterations(10, 1.0, Math.toIntExact(Math.round(0.1 * strategy.populationSize)));
        //replaceAllButNBestsForThresholdAfterMIterations(100, 1.0, 5);
        printStats();
    }

    private Individual tournamentSelectionWithRegardToSpecies(ArrayList<Individual> individualsOfSpecies) {
        ArrayList<Individual> candidates = new ArrayList<Individual>();
        for(int i = 0; i < individualsOfSpecies.size() / 10; i++){
            candidates.add(individualsOfSpecies.get(Parameters.rnd.nextInt(individualsOfSpecies.size())));
        }
        if(candidates.size() == 0){
            return individualsOfSpecies.get(Parameters.rnd.nextInt(individualsOfSpecies.size()));
        }
        return getBest(candidates).copy();
    }

    /**
     * Returns a COPY of the individual selected using tournament selection
     * @return
     */
    private Individual tournamentSelection() {
        ArrayList<Individual> candidates = new ArrayList<Individual>();
        for(int i = 0; i < strategy.tournamentSize; i++){
            candidates.add(population.get(Parameters.rnd.nextInt(population.size())));
        }
        return getBest(candidates).copy();
    }

    // FIXME: *** Crossover ***

    private Individual crossover(Individual parent1, Individual parent2) {

        if(Parameters.rnd.nextDouble() > strategy.crossoverProbability || (!strategy.crossoverTransition && !strategy.crossoverPacing)){
            return parent1;
        }
        Individual child = new Individual(iteration, teamPursuit, strategy, searchArea);

        if(!strategy.crossoverPacing || strategy == Parameters.Strategy.PACING_AVG || strategy == Parameters.Strategy.PACING_MIN_MAX){
            // just copy the pacing strategy from p1 - not evolving in this version
            for(int i = 0; i < parent1.pacingStrategy.length; i++){
                child.pacingStrategy[i] = parent1.pacingStrategy[i];
            }
        }

        if(!strategy.crossoverTransition || strategy == Parameters.Strategy.TRANSITION_TRUES || strategy == Parameters.Strategy.TRANSITION_DISTRIBUTION){
            // just copy the transition strategy from p1 - not evolving in this version
            for(int i = 0; i < parent1.transitionStrategy.length; i++){
                child.transitionStrategy[i] = parent1.transitionStrategy[i];
            }
        }

        switch (strategy){
            case PACING_AVG:
            case PACING_MIN_MAX:
                if(strategy.crossoverTransition) child = crossOverTransition(parent1, parent2, child);
                break;
            case TRANSITION_DISTRIBUTION:
            case TRANSITION_TRUES:
                if(strategy.crossoverPacing) child = crossOverPacing(parent1, parent2, child);
                break;
            case DEFAULT:
                if(strategy.crossoverTransition) child = crossOverTransition(parent1, parent2, child);
                if(strategy.crossoverPacing) child = crossOverPacing(parent1, parent2, child);
                break;
        }

        validateSpecies(child);

        return child;
    }

    private Individual crossOverTransition(Individual parent1, Individual parent2, Individual child){
        int crossoverPointTransition = Parameters.rnd.nextInt(parent1.transitionStrategy.length);
        for(int i = 0; i < crossoverPointTransition; i++){
            child.transitionStrategy[i] = parent1.transitionStrategy[i];
        }
        for(int i = crossoverPointTransition; i < parent2.transitionStrategy.length; i++){
            child.transitionStrategy[i] = parent2.transitionStrategy[i];
        }

        child.attemptToFixTransitionAccordingToSearchArea();

        return child;
    }

    private Individual crossOverPacing(Individual parent1, Individual parent2, Individual child){
        int crossoverPointPacing = Parameters.rnd.nextInt(parent1.pacingStrategy.length);
        for(int i = 0; i < crossoverPointPacing; i++){
            child.pacingStrategy[i] = parent1.pacingStrategy[i];
        }
        for(int i = crossoverPointPacing; i < parent2.pacingStrategy.length; i++){
            child.pacingStrategy[i] = parent2.pacingStrategy[i];
        }

        child.attemptToFixPacingAccordingToSearchArea();

        return child;
    }


    // FIXME: *** Mutation ***

    private Individual mutate(Individual child) {
        if(Parameters.rnd.nextDouble() > strategy.mutationProbability){
            return child;
        }

        switch (strategy){
            case PACING_AVG:
                if(strategy.mutateTransition) child.mutateTransitionByFlippingRandom(false, strategy.minMutation, strategy.maxMutations);
                if(strategy.mutatePacing) child.mutatePacingByApplyingOffspring(true, Integer.MIN_VALUE, Integer.MAX_VALUE, strategy.minMutation, strategy.maxMutations);
                break;
            case PACING_MIN_MAX:
                int[] maxMin = child.calculateMaxAndMinPacingDifference();
                if(strategy.mutateTransition) child.mutateTransitionByFlippingRandom(false, strategy.minMutation, strategy.maxMutations);
                if(strategy.mutatePacing) child.mutatePacingByApplyingOffspring(false, maxMin[1], maxMin[0], strategy.minMutation, strategy.maxMutations);
                break;
            case TRANSITION_TRUES:
                if(strategy.mutateTransition) child.mutateTransitionByFlippingRandom(true, strategy.minMutation, strategy.maxMutations);
                if(strategy.mutatePacing) child.mutatePacingByApplyingOffspring(false, Integer.MIN_VALUE, Integer.MAX_VALUE, strategy.minMutation, strategy.maxMutations);
                break;
            case TRANSITION_DISTRIBUTION:
                if(strategy.mutatePacing) child.mutatePacingByApplyingOffspring(false, Integer.MIN_VALUE, Integer.MAX_VALUE, strategy.minMutation, strategy.maxMutations);
                break;
            case DEFAULT:
                if(strategy.mutateTransition) child.mutateTransitionByFlippingRandom(false, strategy.minMutation, strategy.maxMutations);
                if(strategy.mutatePacing) child.mutatePacingByApplyingOffspring(false, Integer.MIN_VALUE, Integer.MAX_VALUE, strategy.minMutation, strategy.maxMutations);
                break;
        }
        validateSpecies(child);
        return child;
    }

    private void validateSpecies(Individual individual){
        int previousTransitionStrategy = individual.transitionStrategySpecies;
        int previousPacingStrategy = individual.pacingStrategySpecies;
        individual.setSpecies();

        try {
            switch (strategy){
                case PACING_AVG:
                case PACING_MIN_MAX:
                    if(previousPacingStrategy != individual.pacingStrategySpecies && previousPacingStrategy != Integer.MIN_VALUE){
                        throw new Exception("Pacing Strategy does not match after mutation anymore!");
                    }
                case TRANSITION_TRUES:
                case TRANSITION_DISTRIBUTION:
                    if(previousTransitionStrategy != individual.transitionStrategySpecies && previousPacingStrategy != Integer.MIN_VALUE){
                        throw new Exception("Transition Strategy does not match after mutation anymore!");
                    }
            }
        }catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    // FIXME: *** Replacement ***

    private void forceReplaceByRandom(Individual child) {
        int index = Parameters.rnd.nextInt(population.size());
        population.set(index, child);
    }

    private void forceReplaceByAge(Individual child) {
        ArrayList<Individual> oldests = population.stream().sorted(Comparator.comparing(individual -> individual.iterationBorn)).collect(Collectors.toCollection(ArrayList::new));
        double bestResult = getBest(population).result.getFinishTime();
        for(int i = 0; i < oldests.size(); i++){
            if(oldests.get(i).result.getFinishTime() > bestResult){
                int index = population.indexOf(oldests.get(i));
                population.set(index, child);
                i = oldests.size();
            }
        }
    }

    private void replaceAllButNBestsForThresholdAfterMIterations(int n, Double threshold, int iterations){
        if(iteration < iterations){
            return;
        }
        Double currentBest = getBest(population).getFitness();
        int index = iteration - iterations;
        Double bestPreNIterations = bestResultsOfEA.get(index).getFitness();
        Double diff = Math.abs(currentBest - bestPreNIterations);
        if(diff <= threshold){
            ArrayList<Individual> bests = population.stream().sorted(Comparator.comparing(individual -> individual.result.getFinishTime())).collect(Collectors.toCollection(ArrayList::new));
            int popSize = population.size();
            int replacements = 0;
            for(int i = n; i < bests.size(); i++){
                Individual random = new Individual(iteration, teamPursuit, strategy, searchArea);
                random.initialise(strategy.randomiseTransition, segements, strategy.randomisePacing, segements);
                random.evaluate(teamPursuit, false);
                int indexInPop = population.indexOf(bests.get(i));
                population.set(indexInPop, random);
                replacements++;
            }
            System.out.println("Replacement of all but n bests - Replaced " + replacements + " of pop size " + popSize);
        }
    }

	private void replace(Individual child) {
		Individual worst = getWorst(population);
		if(child.getFitness() <= worst.getFitness()){
			int idx = population.indexOf(worst);
			population.set(idx, child);
            //System.out.println("Replaced " + worst.getFitness() + " with " + child.getFitness());
		}
	}

    private void replaceInSpecies(Individual child, ArrayList<Individual> individualsInSpecies) {
        Individual worst = getWorst(individualsInSpecies);
        if(child.getFitness(false) < worst.getFitness(false)){
            int idx = population.indexOf(worst);
            population.set(idx, child);
        }
    }

	private void replaceOldestIfStuckForNIterations(int n, Double threshold, int replacement){
        if(iteration < n){
            return;
        }
        Double currentBest = getBest(population).getFitness();
        int index = iteration - n;
        Double bestPreNIterations = bestResultsOfEA.get(index).getFitness();
        Double diff = Math.abs(currentBest - bestPreNIterations);
        if(diff <= threshold){
            for(int i = 0; i < replacement; i++){
                Individual random = new Individual(iteration, teamPursuit, strategy, searchArea);
                random.initialise(strategy.randomiseTransition, segements, strategy.randomisePacing, segements);
                random.evaluate(teamPursuit, false);
                //forceReplaceByRandom(random);
                forceReplaceByAge(random);
            }
            System.out.println("Replaced " + replacement + " individuals as diff was " + diff + " after " + n + " iterations.");
        }
    }

    // FIXME: *** Helpers ***

    private ArrayList<Individual> getIndividualsInRangeForSharedFitness(int speciesValue){
        switch (strategy){
            case PACING_MIN_MAX:
            case PACING_AVG:
               return new ArrayList<>(population.stream().filter(individual ->  {
                    return individual.pacingStrategySpecies >= (speciesValue - strategy.radius) && individual.pacingStrategySpecies < (speciesValue + strategy.radius);
                }).collect(Collectors.toList()));
            case TRANSITION_TRUES:
            case TRANSITION_DISTRIBUTION:
                return new ArrayList<>(population.stream().filter(individual ->  {
                    return individual.transitionStrategySpecies >= (speciesValue - strategy.radius) && individual.transitionStrategySpecies < (speciesValue + strategy.radius);
                }).collect(Collectors.toList()));
        }
        return null;
    }

    private static Individual getOverallBest(ArrayList<Individual> results) {
        Individual best = results.get(0);
        for(int i = 1; i < results.size(); i++){
            if(results.get(i).result.getFinishTime() < best.result.getFinishTime()){
                best = results.get(i);
            }
        }
        return best;
    }

    private static void printBestResults(ArrayList<Individual> results) {
        for(int i = 0; i < results.size(); i++){
            String str = "";
            for (boolean value : results.get(i).transitionStrategy) {
                str = str + value + ",";
            }
            System.out.println(results.get(i).getFitness() + ";" + str + "\n");
        }
    }

    private void printStats() {
        System.out.println("" + iteration + "\t" + getBest(population).getFitness(false) + "\t" + getWorst(population).getFitness(false));
    }


    private Individual getBest(ArrayList<Individual> aPopulation) {
        double bestFitness = Double.MAX_VALUE;
        Individual best = null;
        for(Individual individual : aPopulation){
            if(individual.getFitness(false) < bestFitness || best == null){
                best = individual;
                bestFitness = best.getFitness(false);
            }
        }
        return best;
    }

    private Individual getWorst(ArrayList<Individual> aPopulation) {
        double worstFitness = 0;
        Individual worst = null;
        for(Individual individual : population){
            if(individual.getFitness(false) > worstFitness || worst == null){
                worst = individual;
                worstFitness = worst.getFitness(false);
            }
        }
        return worst;
    }

    private void printPopulation() {
        for(Individual individual : population){
            System.out.println(individual);
        }
    }

    private void printFintessDistribution(){
        int belowTen = (int) population.stream().map(Individual::getFitness).filter(fitness -> fitness < 210).count();
        int tenToFifteen = (int) population.stream().map(Individual::getFitness).filter(fitness -> fitness >= 210 && fitness < 215).count();
        int fifteenToTwenty = (int) population.stream().map(Individual::getFitness).filter(fitness -> fitness >= 215 && fitness < 220).count();
        int twentyToTwentyFive = (int) population.stream().map(Individual::getFitness).filter(fitness -> fitness >= 220 && fitness < 225).count();
        int twentyFiveToThirty = (int) population.stream().map(Individual::getFitness).filter(fitness -> fitness >= 225 && fitness < 230).count();
        int overThirty = (int) population.stream().map(Individual::getFitness).filter(fitness -> fitness >= 230 && fitness < 1000).count();
        int dnf = (int) population.stream().map(Individual::getFitness).filter(fitness -> fitness >= 1000).count();

        System.out.println("Fitness Distribution: " + belowTen + " - " + tenToFifteen + " - " + fifteenToTwenty + " - " + twentyToTwentyFive + " - " + twentyFiveToThirty + " - " + overThirty + " - " + dnf);
    }

    private static void exploreLandscape() {
        ArrayList<List<Boolean>> randomTransitionStrategies = Lab.generateTransitionStrategies();
        ArrayList<int[]> randomPacingStrategies = Lab.generatePacingStrategies();
        ArrayList<String> results = new ArrayList<>();
        ArrayList<String> resultsPacingStrategies = new ArrayList<>();
        ArrayList<String> resultsTransitionStrategies = new ArrayList<>();
        ArrayList<String> resultsRemaining = new ArrayList<>();
        randomTransitionStrategies.forEach(random -> {
            boolean[] strategy = Lab.toPrimitiveArray(random);
            randomPacingStrategies.forEach(pacing -> {
                try {
                    SimulationResult result = teamPursuit.simulate(strategy, pacing);
                    results.add("" + result.getFinishTime());
                    resultsTransitionStrategies.add("" + random.toString());
                    String str = "";
                    for (int value : pacing) {
                        str = str + value + ",";
                    }
                    resultsPacingStrategies.add("" + str);
                    resultsRemaining.add("" + result.getProportionCompleted());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

        });
        System.out.println("done");
    }
}
