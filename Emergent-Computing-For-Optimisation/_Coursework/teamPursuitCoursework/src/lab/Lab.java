package lab;

import java.util.*;

public class Lab {

    public static ArrayList<List<Boolean>> generateTransitionStrategies() {
        ArrayList<List<Boolean>> randomValues = new ArrayList<List<Boolean>>();
        while(randomValues.size() < 500){
            int length = 22;
            Boolean[] values = new Boolean[length];
            Random rnd = new Random();
            int numberOfTrues = rnd.ints(1, 0, length).toArray()[0];
            for(int i = 0; i < numberOfTrues; i++){
                values[i] = true;
            }
            for(int i = numberOfTrues; i < length; i++){
                values[i] = false;
            }
            List<Boolean> valueList = Arrays.asList(values);
            int i = 0;
            while(i < 100) {
                Collections.shuffle(valueList);
                if(!randomValues.contains(valueList)){
                    randomValues.add(valueList);
                    i = 100;
                }else{
                    i++;
                }
            }
        }
        return randomValues;
    }

    public static ArrayList<int[]> generatePacingStrategies() {
        ArrayList<int[]> randomValues = new ArrayList<int[]>();
        while(randomValues.size() < 500){
            int min = 200;
            int max = 400;
            Random rnd = new Random();
            int[] pacing = rnd.ints(23, min, max).toArray();
            if(!randomValues.contains(pacing)){
                randomValues.add(pacing);
            }
        }
        return randomValues;
    }

    public static boolean[] toPrimitiveArray(final List<Boolean> booleanList) {
        final boolean[] primitives = new boolean[booleanList.size()];
        int index = 0;
        for (Boolean object : booleanList) {
            primitives[index++] = object;
        }
        return primitives;
    }

    public static Boolean[] toNotPrimitiveArray(final boolean[] booleanArray) {
        final Boolean[] primitives = new Boolean[booleanArray.length];
        for(int i = 0; i < booleanArray.length; i++){
            primitives[i] = booleanArray[i];
        }
        return primitives;
    }

}
