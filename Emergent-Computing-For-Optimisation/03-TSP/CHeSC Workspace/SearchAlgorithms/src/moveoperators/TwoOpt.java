package moveoperators;

public class TwoOpt {

    // twoOpt
    // this is a well known heuristic for solving TSPS.
    // it randomly selects two edges in the tour, e.g. (a-b) and (c-d)
    // breaks them, and then reconnects them in a different way, e.g
    // (a-c),(b-d) or (a-d)(b-c)
    // Essentially, this is performed by randomly picking two points
    // in the solution and the reversing the tour between them


    public static int[] apply(int numCities, int[] newSoln, int[] soln){


        // copy the current solution

        for (int i=0;i<numCities;i++)
            newSoln[i]=soln[i];


        // pick two places
        int place1, place2;

        place1 = (int)(Math.random()*numCities);
        place2 = (int)(Math.random()*numCities);

        // if place2<place1, swap them
        if (place2<place1){
            int swap = place1;
            place1=place2;
            place2=swap;
        }

        // create a temporary array that holds all the values between place1 and place2
        int size = place2-place1+1;
        int[] temp = new int[size];
        for (int i=0;i<size;i++)
            temp[i] = soln[place1+i];

        // now replace the cities in newSoln with those in temp but in reverse order
        int start=0;
        for (int i=place2;i>=place1;i--){
            newSoln[i] = temp[start];
            start++;
        }

        return newSoln;

    }

}
