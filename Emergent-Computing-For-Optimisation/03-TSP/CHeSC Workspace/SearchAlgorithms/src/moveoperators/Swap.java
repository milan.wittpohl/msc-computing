package moveoperators;

public class Swap {

    /*
     * This is a simple neighbourhood function that picks two random points
     * and then swaps the cities
     */


    public static int[] apply(int numCities, int[] newSoln, int[] soln){
        // copy the current solution

        for (int i=0;i<numCities;i++)
            newSoln[i]=soln[i];


        // mutate it

        int place1, place2;

        place1 = (int)(Math.random()*numCities);
        place2 = (int)(Math.random()*numCities);


        int swap = soln[place1];
        newSoln[place1] = soln[place2];
        newSoln[place2] = swap;

        return newSoln;

    }

}
