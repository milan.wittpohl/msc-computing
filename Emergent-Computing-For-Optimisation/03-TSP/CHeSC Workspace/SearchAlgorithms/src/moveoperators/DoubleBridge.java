package moveoperators;

import java.util.Arrays;

public class DoubleBridge {

    public static int[] apply(int numCities, int[] newSoln, int[] soln) {
        // pick eight places
        int[] places = new int[5];
        for(int i=0; i < places.length; i++) {
            places[i] = (int)(Math.random()*numCities);
        }

        // sort places asc
        Arrays.sort(places);

        // s1
        for(int i = places[0]; i < places[1]; i++){
            newSoln[i] = soln[i];
        }
        // s4
        for(int i = places[3]; i < places[4]; i++){
            newSoln[i] = soln[i];
        }
        // s3
        for(int i = places[2]; i < places[3]; i++){
            newSoln[i] = soln[i];
        }
        // s2
        for(int i = places[1]; i < places[2]; i++){
            newSoln[i] = soln[i];
        }
        // rest
        for(int i = 0; i < soln.length; i++){
            if(i < places[0] || i >= places[places.length - 1]){
                newSoln[i] = soln[i];
            }
        }

        return newSoln;
    }

}
