package tspsolver;

import java.io.FileReader;
import java.io.StreamTokenizer;
import java.text.DecimalFormat;

public abstract class AbstractSearchMethod {

    String op;
    boolean verbose;
    int maxIterations;
    int solnLength=5;
    boolean isInitialised=false;
    double currentFitness;
    int numIter;
    int soln[];
    int newSoln[];
    int numCities;
    double xcoord[];
    double ycoord[];
    int NEW=1;
    int OLD=2;

    public void printTour(int which){
        for (int i=0;i<soln.length;i++){
            if (which==OLD)
                System.out.print(soln[i] + "-");
            else
                System.out.print(newSoln[i] + "-");
        }
        System.out.println("\n");
    }

    // this method initialises a solution with
    // a random permutation

    public void initialise(){

        DecimalFormat df = new DecimalFormat("0.00");
        // create an array to hold initial solution and new solutions
        soln= new int[numCities];
        newSoln= new int[numCities];

        // and set it to a random permutation
        permutation();

        currentFitness=getTourLength(soln);
        System.out.println("Start tour length = "+df.format(currentFitness));
        if (verbose) printTour(OLD);
    }

    // this method creates a random permutation  1..N
    public void permutation(){


        // insert integers 1..N
        for (int i = 0; i < numCities; i++)
            soln[i] = i+1;

        // shuffle
        for (int i = 0; i < numCities; i++) {
            int r = (int) (Math.random() * (i+1));     // int between 1 and N
            int swap = soln[r];
            soln[r] = soln[i];
            soln[i] = swap;
        }


    }

    // replace the old soution with the new solution
    public void replaceSolution(){
        for (int j=0;j<numCities;j++)
            soln[j] = newSoln[j];
    }


    public double getTourLength(int []someSolution){
        double fitness=0;

        double x1,x2,y1,y2;
        int k;
        // calculate the total length of the tour

        for (int j=0;j<someSolution.length;j++){
            int id1=someSolution[j];
            if (j==someSolution.length-1)
                k=0;
            else
                k=j+1;
            int id2 = someSolution[k];

            x1=xcoord[id1];
            y1=ycoord[id1];
            x2=xcoord[id2];
            y2=ycoord[id2];


            double dist = Math.sqrt( (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
            fitness+=dist;
        }

        return(fitness);
    }

    // read  in the TSP data

    public void loadData(String myFile){
        System.out.println("file is "+myFile);
        try{

            StreamTokenizer st = new StreamTokenizer(new FileReader(myFile));
            boolean foundDim=false;
            st.wordChars(33,126);
            while (!foundDim){
                // read until we find the DIMENSION:
                int token = st.nextToken();

                if (token == StreamTokenizer.TT_WORD){
                    String theword  = (String)st.sval;
                    if (theword.equals("DIMENSION:"))
                        foundDim=true;
                    else
                        st.nextToken();
                } else
                    st.nextToken();
                if (token == StreamTokenizer.TT_EOF)
                    break;

            }

            // next thing should be a number then (numCities)
            int token = st.nextToken();
            if (st.ttype != StreamTokenizer.TT_NUMBER){
                // error!
                System.out.println ("error ? token is not a number");
                System.exit(0);
            } else {
                numCities = (int)st.nval;
                System.out.println("Num cities is "+numCities);
                xcoord = new double[numCities+1];
                ycoord = new double[numCities+1];
            }

            // now skip until we find the NODE_COORD
            boolean foundCoords=false;

            while (!foundCoords){
                token = st.nextToken();
                if (token == StreamTokenizer.TT_NUMBER){
                    System.out.println ("Error reading file " + st.nval);
                    System.exit(0);
                }

                if (st.ttype== StreamTokenizer.TT_WORD){
                    String theword = (String)st.sval;
                    if (theword.equals("NODE_COORD_SECTION"))
                        foundCoords=true;
                    else
                        st.nextToken();
                } else
                    st.nextToken();

            }

            int i;
            // loop over data then and read coords
            for (i=0;i<numCities;i++){
                st.nextToken();
                int id = (int)st.nval;
                st.nextToken();
                double x = (double)st.nval;
                st.nextToken();
                double y = (double)st.nval;
                xcoord[id] =x;
                ycoord[id]=y;

            }


        }
        catch (Exception e){
            System.out.println("Error reading problem file " + myFile);
        }
    }

}
