package tspsolver;

import moveoperators.DoubleBridge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class IteratedLocalSearch {

    public static ResultDTO run(String fileName, int numRuns){
        // Initial Hill climber run
        hillclimber myHC = new hillclimber(fileName, 10000, false, "TWO_SWAP");
        ArrayList<ResultDTO> results = new ArrayList<ResultDTO>();
        for (int i=0;i<numRuns;i++) {
            ResultDTO result = myHC.runHC();
            results.add(result);
        }
        ResultDTO bestResult = results.stream().min(Comparator.comparing(ResultDTO::getResultLength)).get();
        for(int i = 0; i < numRuns; i++){
            int[] perturbation = DoubleBridge.apply(bestResult.getResult().length, new int[bestResult.getResult().length], bestResult.getResult());
            simanneal mySA = new simanneal(fileName, 10000, 10000, 0, 0.99, false, "TWO_SWAP");
            ArrayList<ResultDTO> newResults = new ArrayList<ResultDTO>();
            for (int n=0;n<numRuns;n++) {
                ResultDTO newResult = mySA.runSA(false, perturbation);
                newResults.add(newResult);
            }
            ResultDTO bestNewResult = newResults.stream().min(Comparator.comparing(ResultDTO::getResultLength)).get();
            // Acceptance
            if(bestNewResult.getResultLength() < bestResult.getResultLength()){
                bestResult = bestNewResult;
            }
        }
        return bestResult;
    }

}
