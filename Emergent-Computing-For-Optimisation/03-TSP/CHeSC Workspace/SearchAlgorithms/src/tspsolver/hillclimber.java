package tspsolver;
/* Simple HillClimber
 * This program reads in a TSP file and attempts to find the minimum tour length
 * using a hill climbing algorithm
 * There are 2 move-operators built in:
 *    swap - this just picks 2 cities at random and swaps
 *           their positions in the tour
 *    twoOpt: this picks cities at random and then reverses the order of the
 *            existing tour between these two city positions.
 *
 *  To Run;
 *      java hillclimber [file] [iterations] [v]
 *
 *  The first parameter is the name of a TSP file
 *  The second parameter is the number of iterations to run the algorithm for
 *  The third parameter is optional. If given, a verbose report is written
 *  as the algorithm runs
 */



import moveoperators.DoubleBridge;
import moveoperators.Swap;
import moveoperators.TwoOpt;

import java.io.*;
import java.text.*;
import java.util.Arrays;

public class hillclimber extends AbstractSearchMethod{
	int flipSize;

	public hillclimber(String myfile, int it, boolean v, String operator){
		// initialise parameters

		maxIterations=it;
		loadData(myfile);
		verbose=v;
		op = operator;
		System.out.println("Solver is HILL CLIMBER");
	}


	public ResultDTO runHC(){

		// neighbourSolution
		double fitness;
		// for formatting
		DecimalFormat df = new DecimalFormat ("0.00");

		// initialise with a random solution
		initialise();

		// run algorithm with parameters chosen
		for (int i=0;i<maxIterations;i++){

			// ******************************************************
			// apply neighbourhood function to get a potential new solution
			// you can replace this method with any of the methods
			// provided, or code your own!
			// *******************************************************

			if (op == "SWAP")
				newSoln = Swap.apply(numCities, newSoln, soln);
			else if (op == "DOUBLE_BRIDGE")
				newSoln = DoubleBridge.apply(numCities, newSoln, soln);
			else
				newSoln = TwoOpt.apply(numCities, newSoln, soln);

			fitness=getTourLength(newSoln);

			// if the new one is better than the old one (shorter distance)
			// then replace it

			if (fitness < currentFitness){
				replaceSolution();
				currentFitness=fitness;
				if (verbose){
					System.out.println("iteration " + i + " found better solution: " + df.format(currentFitness));
				}
			}
		}

		// print  the  final solution
		if (verbose){
			System.out.println("\n\nFinal Solution:");
			printTour(OLD);
		}

		System.out.println("Best tour length " +  df.format(currentFitness));
		return new ResultDTO(currentFitness, newSoln);
	}

}



