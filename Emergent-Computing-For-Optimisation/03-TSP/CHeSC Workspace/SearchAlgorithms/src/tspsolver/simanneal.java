package tspsolver;
/*Simulated Annealing
 * This program reads in a TSP file and attempts to find the minimum tour length
 * using simulated annealing.
 * There are 2 move-operators built in:
 *    swap - this just picks 2 cities at random and swaps
 *           their positions in the tour
 *    twoOpt: this picks cities at random and then reverses the order of the
 *            existing tour between these two city positions.
 *
 *  To Run;
 *      java SimAnneal filename iterations startTemp finalTemp cooolingRate v
 *
 *  The first parameter is the name of a TSP file
 *  The second parameter is the number of iterations to run the algorithm for
 *  The third parameter is the start temperature (e.g 300)
 *  The 4th parameter is the final temperature (e.g 0.0)
 *  The 5th parameter is the cooling rate (normally between 0.90 and 1.0)
 *  The last parameter is optional. If given, a verbose report is written
 *  as the algorithm runs
 */



import moveoperators.DoubleBridge;
import moveoperators.Swap;
import moveoperators.TwoOpt;

import java.io.*;
import java.text.*;
import java.util.Arrays;
import java.util.Collections;

public class simanneal extends AbstractSearchMethod{
	double coolingRate;
	double startTemp;
	double finalTemp;
	double currentTemp;
	String op;


	public simanneal(String myfile, int it, double s, double f, double r, boolean v, String operator){
		// initialise parameters

		maxIterations=it;
		startTemp = s;
		finalTemp = f;
		coolingRate = r;
		loadData(myfile);
		verbose=v;
		op = operator;


		System.out.println("Solver is SIMULATED ANNEALING");
	}

	public ResultDTO runSA(){
		return runSA(true, null);
	}


	public ResultDTO runSA(boolean initialise, int[] currentSolution){

		double fitness;
		int iter=0;
		double randomNum;
		double diff,prob;

		DecimalFormat df = new DecimalFormat ("0.00");

		if(initialise){
			// initialise with a random solution
			initialise();
		}else{
			soln = currentSolution;
			newSoln = new int[soln.length];
			currentFitness=getTourLength(soln);
		}

		currentTemp = startTemp;

		// run algorithm with parameters chosen
		while (iter<maxIterations && currentTemp > finalTemp){

			// ******************************************************
			// apply neighbourhood function to get a potential new solution
			// you can replace this method with any of the methods
			// provided, or code your own!
			//
			//Two alternative mutation methods are provided:
			//  swapTwoCities and twoOpt
			//
			// *******************************************************

			if (op == "SWAP")
				newSoln = Swap.apply(numCities, newSoln, soln);
			else if (op == "DOUBLE_BRIDGE")
				newSoln = DoubleBridge.apply(numCities, newSoln, soln);
			else
				newSoln = TwoOpt.apply(numCities, newSoln, soln);

			fitness=getTourLength(newSoln);

			// if the new one is better than the old one (shorter distance)
			// then replace it

			if (fitness <= currentFitness){
				replaceSolution();
				currentFitness=fitness;
				if (verbose){
					System.out.println("iteration " + iter + " temp " + df.format(currentTemp)+ " found better solution: " + df.format(currentFitness));
				}
			} else {
				// accept worse solution with some probability
				diff = fitness-currentFitness;
				randomNum = Math.random();
				prob = Math.exp(-(double)Math.abs(diff)/currentTemp);


				if (prob>randomNum){
					replaceSolution();
					currentFitness=fitness;
					if (verbose)
						System.out.println("iteration " + iter + " temp " + df.format(currentTemp) + " accepted worse solution " + df.format(currentFitness));
				}


			}

			// decrease temperature
			currentTemp*=coolingRate;

			// increment iteration counter
			iter++;
		}


		// print  the  final solution
		if (verbose){
			System.out.println("\n\nFinal Solution:");
			printTour(OLD);
		}

		System.out.println("Best tour length " +  df.format(currentFitness));
		return new ResultDTO(currentFitness, newSoln);
	}

}



