package tspsolver;

public class ResultDTO {

    private Double resultLength;
    private int[] result;

    public ResultDTO(Double resultLength, int[] result){
        this.resultLength = resultLength;
        this.result = result;
    }

    public Double getResultLength() {
        return resultLength;
    }

    public int[] getResult() {
        return result;
    }

}
